<?php get_header(); ?>

<?php /*START LOOP */ if ( get_posts() ) : while ( have_posts() ) : the_post(); ?>

	<section class="full-width-fluid bordered block">
		<div class="centered block__text">
			<h2><?php the_title(); ?>: Coming Soon</h2>
		</div>
	</section>

<?php /*END LOOP */ endwhile; endif; ?>

<?php get_footer(); ?>