<!DOCTYPE html>

<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->

<html <?php language_attributes(); ?>>

<head>
	
<title>
	<?php wp_title( '-', true, 'right' ); ?>
</title>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div class="nav-placeholder">
	<nav class="nav">

		<?php wp_nav_menu( array( 
			'menu'       => 'Main Menu Left', 
			'container'  => false, 
			'menu_class' => 'nav__menu left', 
			'walker'     => new minimal_navwalker() 
		)); ?>

		<a class="nav__logo-link" href="<?php echo get_home_url(); ?>">
			<img class="nav__logo horizontal" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_Logo_horizontal_blue.svg">
			<img class="nav__logo circle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_Logo_circle_blue.svg">
		</a>

		<?php wp_nav_menu( array( 
			'menu'       => 'Main Menu Right', 
			'container'  => false, 
			'menu_class' => 'nav__menu right', 
			'walker'     => new minimal_navwalker() 
		)); ?>

		<div class="nav__hamburger">
		</div>

	</nav>
</div>