<?php
/*
Template Name: The Building
*/

get_header(); 

echo get_template_part('snippet-hero-image');

?>

<section class="centered building-subnav">
	<ul class="building-subnav__list">
		<li class="building-subnav__link active"><a href="<?php echo home_url('building'); ?>"><h4>The Building</h4></a></li>
		<li class="building-subnav__link"><a href="<?php echo home_url('floor-plans'); ?>"><h4>Floor Plans</h4></a></li>
		<li class="building-subnav__link"><a href="<?php echo home_url('the-team'); ?>"><h4>The Team</h4></a></li>
	</ul>
</section>

<section id="redefine" class="full-width block">
	<div class="centered block__text">
		<h1>Redefining The<br/>Work Space</h1>
		<p class="big">25 Kent is built for the way we live and work today—and it’s like nothing you’ve ever seen before. Floor-to-ceiling windows, an expansive rooftop, terraces on every level, and a public thoroughfare welcome all of Williamsburg to your front door.</p>
		<a class="button" href="<?php echo get_stylesheet_directory_uri(); ?>/pdf/25KentSpecSheet.pdf" target="_blank"><span class="button__text">Get Spec Sheet</span></a>
	</div>
	<hr class="building-divider"/>
</section>

<section class="centered full-width block">
	<div class="block__text">
		<h4>Introducing</h4>
		<h2>Twenty Five Kent</h2>
	</div>
	<div class="building-carousel">
		<div class="building-carousel__container">
			<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/slideshow-placeholder.jpg" /></div>
			<div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/slideshow-placeholder-1.jpg" /></div>
		</div>
		<div class="building-carousel__controls">
			<a href="javascript:void(0)" class="next"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
			<a href="javascript:void(0)" class="prev"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
		</div>
	</div>
</section>

<section class="full-width bordered gray block">
	<div class=" centered building-highlights">
		<h4>Highlights</h4>
		<h2>We Have What You Want</h2>
		<p>Built from the ground up on Williamsburg’s revitalized waterfront, 25 Kent is Brooklyn’s first new office and production space in over 40 years and no detail was overlooked.</p>
		
		<?php if( have_rows('hightlights_items') ): ?>

			<ul class="building-highlights__container">

			<?php while( have_rows('hightlights_items') ): the_row(); 

				$icon = get_sub_field('icon');
				$headline = get_sub_field('highlight_headline');
				$copy = get_sub_field('highlight_copy');

				?>

				<li class="building-highlights__item">

					<?php if( $icon ): ?>
						<div class="image">
							<img src="<?php echo esc_url($icon); ?>" />
						</div>
					<?php endif; ?>

					<?php if( $headline ): ?>
						<h4><?php echo esc_html($headline); ?></h4>
					<?php endif; ?>

				 <?php if( $copy ): ?>
				 	<p><?php echo esc_html($copy); ?></p>
				 <?php endif; ?>

				</li>

			<?php endwhile; ?>

			</ul>

		<?php endif; ?>

		<hr class="building-divider"/>
		<h4 class="navy">Tax Benefits</h4>
		<p class="big">Get paid to move to Brooklyn. <br/>Available tax benefits of up to $15 PSF.</p>

	</div>
</section>

<section id="details" class="full-width-fluid bordered navy block">
	<div class="building-details">
		<h4>Building Overview</h4>
		<h2>Need-To-Knows</h2>

		<?php if( have_rows('details_items') ): ?>

			<ul class="building-details__container">

			<?php while( have_rows('details_items') ): the_row(); 

				$label = get_sub_field('details_label');
				$items = get_sub_field('details_item');

				?>

				<li class="building-details__item">

					<?php if( $label ): ?>
							<h3><?php echo esc_html($label); ?></h3>
					<?php endif; ?>

					<?php if( $items ): ?>
						<?php echo wpautop($items); ?>
					<?php endif; ?>

				</li>

			<?php endwhile; ?>

			</ul>

		<?php endif; ?>

	</div>
</section>

<section id="explore" class="full-width-fluid block gray">
	<div class="building-explore">
		<div class="building-explore__text">
			<h4>Cross-Section</h4>
			<h2>Explore The Space</h2>
			<hr>
			<p>We’re building an environment with you in mind. Perks like underground parking and bike storage make it easy to get to work. Natural light, centralized collaborative areas, and ample outdoor space make it easy to get inspired.</p>
		</div>
		<div class="building-explore__space">

			<div class="building-marker roof">
    <a href="javascript:void(0)">
      <span class="building-marker__counter">1</span>
      <span class="building-marker__content">
       <span class="text">Rooftop Terrace</span>
      </span>
    </a>
  	</div>

  	<div class="building-marker terrace">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">2</span>
     <span class="building-marker__content">
      <span class="text">Private Terraces</span>
     </span>
    </a>
  	</div>

  	<div class="building-marker light">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">3</span>
     <span class="building-marker__content">
      <span class="text">Light Manufacturing</span>
     </span>
    </a>
  	</div>

  	<div class="building-marker common">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">4</span>
     <span class="building-marker__content">
      <span class="text">Common Areas</span>
     </span>
    </a>
  	</div>

  	<div class="building-marker parking">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">5</span>
     <span class="building-marker__content">
      <span class="text">Underground Parking</span>
     </span>
    </a>
  	</div>

  	<div class="building-marker retail">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">6</span>
     <span class="building-marker__content">
      <span class="text">Retail Thoroughfare</span>
     </span>
    </a>
  	</div>

  	<div class="building-marker plaza">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">7</span>
     <span class="building-marker__content">
      <span class="text">Two Public Plazas</span>
     </span>
    </a>
  	</div>

  	<div class="building-marker bike">
    <a href="javascript:void(0)">
     <span class="building-marker__counter">8</span>
     <span class="building-marker__content">
      <span class="text">Bike Storage</span>
     </span>
    </a>
  	</div>

			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K-Building-Explore.svg" />
		</div>

		<div class="building-explore__mobile">
			<ul>
				<li>1. Private Terraces</li>
				<li>2. Underground Parking</li>
				<li>3. Light Manufacturing</li>
				<li>4. Rooftop Terrace</li>
				<li>5. Retail Thoroughfare</li>
				<li>6. Common Areas</li>
				<li>7. Two Public Plazas</li>
				<li>8. Bike Storage</li>
			</ul>
		</div>
	</div>
</section>

<div class="clearfix">

	<div class="half-width block">
		<div class="middle centered block__text">
			<h4>Floor plans</h4>
			<h2>Fit In Perfectly</h2>
			<hr>
			<p>We offer eight levels of flexible floor plans. Explore floor-by-floor and sample test 
fits to see single, double, and multi-tenant capabilities.</p>
			<a class="button" href="<?php echo home_url('floor-plans'); ?>">
				<span class="button__text">See What Fits</span>
			</a>
		</div>
	</div>

	<div class="half-width bordered image block" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/floorplan.png');">
	</div>

</div>

<div class="half-width bordered image block" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/theteam.jpg');">
</div>

<div class="half-width block">
	<div class="middle centered block__text">
		<h4>The Team</h4>
		<h2>Get To Know Us</h2>
		<hr>
		<p>Meet the personalities behind 25 Kent. We’re locally-backed institutional developers with a long history of investing in Brooklyn’s future.</p>
		<a class="button" href="<?php echo home_url('the-team'); ?>">
			<span class="button__text">Meet Us</span>
		</a>
	</div>
</div>


<?php

echo get_template_part('snippet-footer-cta');

get_footer();