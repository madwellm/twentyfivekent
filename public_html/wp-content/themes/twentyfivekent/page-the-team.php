<?php 
/*
Template Name: Team Page
*/

get_header();
?>

<section class="full-width-fluid bordered navy block contact-hero">
	<div class="contact-hero__text block__text">
		<h1 class="contact-hero__header">
			<?php if ( get_field('hero_title') ) echo esc_html(get_field('hero_title')); ?>
		</h1>	
		<p class="big contact-hero__paragraph">
			<?php if ( get_field('hero_description') ) echo esc_html(get_field('hero_description')); ?>
		</p>
	</div>
</section>

<section class="centered building-subnav">
	<ul class="building-subnav__list">
		<li class="building-subnav__link"><a href="<?php echo home_url('building'); ?>"><h4>The Building</h4></a></li>
		<li class="building-subnav__link"><a href="<?php echo home_url('floor-plans'); ?>"><h4>Floor Plans</h4></a></li>
		<li class="building-subnav__link active"><a href="<?php echo home_url('the-team'); ?>"><h4>The Team</h4></a></li>
	</ul>
</section>

<section class="team-partners">
	<div class="half-width block">
		<div class="middle block__text">

		<?php if( have_rows('partners_bios') ): ?>

			<?php while( have_rows('partners_bios') ): the_row(); 

				$name = get_sub_field('partner_name');
				$bio = get_sub_field('partner_bio');

				?>

				<div class="team-partners__bio">

				<?php if( $name ): ?>
					<h2><?php echo esc_html( $name ); ?></h2>
				<?php endif; ?>

				<hr/>

				<?php if( $bio ): ?>
					<?php echo wpautop( esc_html( $bio )); ?>
				<?php endif; ?>

				</div>

			<?php endwhile; ?>

		<?php endif; ?>

		</div>
	</div>
</section>

<section class="team-logos">
	<div class="half-width block">
		<div class="middle block__text">

		<?php if( have_rows('team_logos') ): ?>

			<ul class="team-logos__list">

			<?php while( have_rows('team_logos') ): the_row(); 

				$teamtitle = get_sub_field('team_title');
				$teamlogo = get_sub_field('team_logo');

				?>

				<li class="team-logos__item">

				<?php if( $teamtitle ): ?>
					<h4><?php echo esc_html( $teamtitle ); ?></h4>
				<?php endif; ?>

				<?php if( $teamlogo ): ?>
					<img src="<?php echo esc_url( $teamlogo ); ?>" />
				<?php endif; ?>

				</li>

			<?php endwhile; ?>

			</ul>

		<?php endif; ?>

		</div>
	</div>
</section>

<section class="full-width bordered hr block">
	<hr class="full">
</section>

<?php 
if ( get_field('logo_image') ) $logo = get_field('logo_image');

if ( get_field('agent_1_phone_number') ) $phone_1 = esc_html(get_field('agent_1_phone_number'));
if ( get_field('agent_1_e-mail') ) $email_1 = esc_html(get_field('agent_1_e-mail'));

if ( get_field('agent_2_phone_number') ) $phone_2 = esc_html(get_field('agent_2_phone_number'));
if ( get_field('agent_2_e-mail') ) $email_2 = esc_html(get_field('agent_2_e-mail'));

if ( get_field('agent_3_phone_number') ) $phone_3 = esc_html(get_field('agent_3_phone_number'));
if ( get_field('agent_3_e-mail') ) $email_3 = esc_html(get_field('agent_3_e-mail'));

?>

<section id="team-inquiries" class="full-width-fluid bordered block contact-inquiries">
	<img class="contact-inquiries__logo"src="<?php if ( isset($logo) ) echo esc_url($logo['sizes']['hero']); ?>" alt="mark">
	<hr>
	<h2 class="contact-inquiries__header"><?php if ( get_field('tenant_inquiries') ) echo esc_html(get_field('tenant_inquiries')); ?></h2>
	<p class="contact-inquiries__subhead"><?php if ( get_field('tenant_inquiries_sub') ) echo wpautop( esc_html( get_field('tenant_inquiries_sub'))); ?></p>

	<div class="col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('agent_1') ) echo esc_html(get_field('agent_1')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php echo str_replace(".","",$phone_1); ?>" class="tel"><?php echo $phone_1; ?></a>
		</p>
		<p class="contact-inquiries__paragraph">
			<a href="mailto:<?php echo $email_1; ?>" target="_blank"><?php echo $email_1; ?></a>
		</p>
	</div>

	<div class="col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('agent_1') ) echo esc_html(get_field('agent_2')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php echo str_replace(".","",$phone_2); ?>" class="tel"><?php echo $phone_2; ?></a>
		</p>
		<p class="contact-inquiries__paragraph">
			<a href="mailto:<?php echo $email_2; ?>" target="_blank"><?php echo $email_2; ?></a>
		</p>
	</div>

	<div class="col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('agent_1') ) echo esc_html(get_field('agent_3')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php echo str_replace(".","",$phone_3); ?>" class="tel"><?php echo $phone_3; ?></a>
		</p>
		<p class="contact-inquiries__paragraph">
			<a href="mailto:<?php echo $email_3; ?>" target="_blank"><?php echo $email_3; ?></a>
		</p>
	</div>

</section>

<?php
get_footer();
