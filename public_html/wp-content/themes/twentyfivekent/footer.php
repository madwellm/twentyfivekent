
<footer class="footer">
	<section class="centered block footer__hat">
		<h5>Need a hard copy? CLick to download the 25 Kent spec sheet. <a class="footer__spec-link" href="<?php echo get_stylesheet_directory_uri(); ?>/pdf/25KentSpecSheet.pdf" target="_blank"> Get Spec Sheet</a></h5>
	</section>
	<section class="navy block footer__body">

		<div class="row">
			<div class="col-2">
				<?php wp_nav_menu( array( 
					'menu'       => 'Footer Menu Left', 
					'container'  => false, 
					'menu_class' => 'footer__menu', 
					'walker'     => new minimal_navwalker() 
				)); ?>
			</div>
			<div class="col-2">
				<?php wp_nav_menu( array( 
					'menu'       => 'Footer Menu Center', 
					'container'  => false, 
					'menu_class' => 'footer__menu', 
					'walker'     => new minimal_navwalker() 
				)); ?>
			</div>
			<div class="col-2">
				<?php wp_nav_menu( array( 
					'menu'       => 'Footer Menu Right', 
					'container'  => false, 
					'menu_class' => 'footer__menu', 
					'walker'     => new minimal_navwalker() 
				)); ?>
			</div>
			<div class="col-4 footer__contact">
				<h5 class="footer__contact-header">Contact Leasing Agent</h5>
				<a class="tel footer__contact-link" href="tel:6464413706">646.441.3706</a>
				<br>
				<a class="footer__contact-link" href="mailto:hello@twentyfivekent.com" target="_blank">hello@twentyfivekent.com</a>
			</div>
			<div class="col-2 footer__logo-col">
				<a class="footer__logo-link" href="<?php echo get_home_url(); ?>">
					<img class="footer__logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_Logo_circle_white.svg">
				</a>
			</div>
		</div>

	</section>
	<section class="coral block footer__boots">
		<p class="footer__credits">Project by Rubenstein Partners and Heritage Equity Partners</p>
	</section>
</footer>

<div class="nav-overlay">
	<!-- NAV SUB-MENU CLONES HERE -->
</div>

<div class="uber-overlay">
	<!-- ON TOP OF EVERYTHING, VIDEO GOES HERE -->
</div>

<!-- START SCRIPTS -->

<?php wp_footer(); ?>

<?php if ( SERVER_ENVIRONMENT == 'dev' ) { ?>
	<!-- BugHerd -->
	<script type='text/javascript'>
	(function (d, t) {
	  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	  bh.type = 'text/javascript';
	  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=uwc7nmbdwddpozz1rmnveq';
	  s.parentNode.insertBefore(bh, s);
	  })(document, 'script');
	</script>
	<!-- End BugHerd -->
<?php } ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TZNST7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TZNST7');</script>
<!-- End Google Tag Manager -->

</body>
</html>