<svg 
	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
	 x="0px" y="0px" width="1427px" height="1787px" class="neighborhood-hotspots__markers" viewBox="0 0 1427 1787" style="enable-background:new 0 0 1427 1787;"
	 xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{fill:#DD6B6B;}
	.st2{fill:#88CCE5;}
	.st3{fill:#A09CB4;}
	.st4{fill:#EE5754;}
	.st5{fill:#191345;}
	.st6{fill:#B23B3C;}
</style>
<defs>
</defs>
<g>
	<g>
		<g class="neighborhood-hotspots__marker" data-hotspot="The Williamsburg Hotel">
			<polyline class="st1" points="696.3,856.7 707.6,837 719,856.7 			"/>
			<polygon class="st1" points="718.9,856.8 707.6,837.4 696.4,856.8 696.1,856.6 707.6,836.7 719.2,856.6 			"/>
		</g>
		<g class="neighborhood-hotspots__marker" data-hotspot="The Hoxton Williamsburg">
			<polyline class="st1" points="714.8,899.6 726.2,879.9 737.6,899.6 			"/>
			<polygon class="st1" points="737.4,899.7 726.2,880.2 715,899.7 714.7,899.5 726.2,879.5 737.7,899.5 			"/>
		</g>
		<g class="neighborhood-hotspots__marker" data-hotspot="Wythe Hotel">
			<polyline class="st1" points="746.5,814.7 757.9,795 769.3,814.7 			"/>
			<polygon class="st1" points="769.1,814.8 757.9,795.4 746.7,814.8 746.4,814.6 757.9,794.6 769.4,814.6 			"/>
		</g>
		<g class="neighborhood-hotspots__marker" data-hotspot="The William Vale Hotel">
			<polyline class="st1" points="801.1,810.5 812.5,790.8 823.8,810.5 			"/>
			<polygon class="st1" points="823.7,810.6 812.5,791.2 801.3,810.6 800.9,810.4 812.5,790.4 824,810.4 			"/>
		</g>
		<g class="neighborhood-hotspots__marker" data-hotspot="McCarren Hotel &amp; Pool">
			<polyline class="st1" points="831.7,891.5 843.1,871.8 854.5,891.5 			"/>
			<polygon class="st1" points="854.3,891.6 843.1,872.1 831.9,891.6 831.6,891.4 843.1,871.4 854.6,891.4 			"/>
		</g>
		<g>
			<g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Kent Ale House">
					<circle class="st2" cx="690.2" cy="795" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Ides">
					<circle class="st2" cx="746.9" cy="823" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Brooklyn Bowl">
					<circle class="st2" cx="776.2" cy="825.4" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Whiskey Brooklyn">
					<circle class="st2" cx="797.4" cy="867.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Craic">
					<circle class="st2" cx="879.1" cy="1025.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Richlane">
					<circle class="st2" cx="991.4" cy="1054.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Pete’s Candy Store">
					<circle class="st2" cx="1093.3" cy="1057" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Shanty">
					<circle class="st2" cx="1158.8" cy="1023.4" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Night of Joy">
					<circle class="st2" cx="1101" cy="1118.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Exley">
					<circle class="st2" cx="1038.3" cy="1163.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Sugarburg">
					<circle class="st2" cx="1050.9" cy="1274.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Union Pool">
					<circle class="st2" cx="1053" cy="1223.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Commodore">
					<circle class="st2" cx="848.4" cy="1305.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Rocka Rolla">
					<circle class="st2" cx="984.2" cy="1305.3" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Spritzenhaus">
					<circle class="st2" cx="838.5" cy="1274.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Sort of Wine Bar">
					<circle class="st2" cx="736.2" cy="1238.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Four Horsemen">
					<circle class="st2" cx="777.6" cy="1344.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Brooklyn Winery">
					<circle class="st2" cx="882.7" cy="1101.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Abbey Bar">
					<circle class="st2" cx="826.8" cy="1089.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Pearl’s">
					<circle class="st2" cx="811.9" cy="1070.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Ramona">
					<circle class="st2" cx="595" cy="1134.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Skinny Dennis">
					<circle class="st2" cx="558.2" cy="1186.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Nighthawk Cinema">
					<circle class="st2" cx="540.1" cy="1177.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Grand Ferry Tavern">
					<circle class="st2" cx="423.7" cy="1139.8" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Black Bear Brooklyn">
					<circle class="st2" cx="580.1" cy="992.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Rosemary’s Greenpoint Tavern">
					<circle class="st2" cx="738.8" cy="1080.4" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Richardson">
					<circle class="st2" cx="1330.1" cy="1006.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Hotel Delmano">
					<circle class="st2" cx="738.9" cy="957.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="George &amp; Jack’s Tap Room">
					<circle class="st2" cx="735" cy="1008.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="D.O.C. Wine Bar">
					<circle class="st2" cx="637.6" cy="956.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Counting Room">
					<circle class="st2" cx="812.5" cy="878.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Berry Park">
					<circle class="st2" cx="874.4" cy="799" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Northern Territory">
					<circle class="st2" cx="777.8" cy="629.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Diamond">
					<circle class="st2" cx="773.4" cy="540.3" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Black Rabbit">
					<circle class="st2" cx="810.3" cy="338" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Ramona">
					<circle class="st2" cx="752" cy="334.4" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Manhattan Inn">
					<circle class="st2" cx="1080.4" cy="701.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Torst">
					<circle class="st2" cx="1090.4" cy="731.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="No Name Bar">
					<circle class="st2" cx="1099.7" cy="749.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Alligator Lounge">
					<circle class="st2" cx="1156.3" cy="1305.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Bar Matchless">
					<circle class="st2" cx="1118" cy="793.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Barcade">
					<circle class="st2" cx="1060" cy="1415.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Zablozki’s">
					<circle class="st2" cx="662.5" cy="1028.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Brooklyn Brewery">
					<circle class="st2" cx="790.1" cy="836.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Camlin">
					<circle class="st2" cx="494.1" cy="1054.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Gibson">
					<circle class="st2" cx="866.3" cy="921.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Falcon Laundry Bar &amp; Restaurant">
					<circle class="st2" cx="610.4" cy="931.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Kent Ale House">
					<circle class="st2" cx="690.2" cy="795" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Levee">
					<circle class="st2" cx="580.2" cy="1159.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Okonomi // YUJI">
					<rect x="1155.7" y="1380.1" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Brooklyn Star">
					<rect x="1135" y="1254.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Bourbon Springs">
					<rect x="1312.9" y="1070.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Battery Harris">
					<rect x="1129.2" y="1083.9" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Beco">
					<rect x="1076.6" y="1021.8" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Lilia">
					<rect x="1020" y="1096.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Reunion">
					<rect x="987.3" y="1070.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Jimmy’s Diner">
					<rect x="1015" y="1057" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Santa Salsa">
					<rect x="1011.6" y="1029.3" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="St. Mazie">
					<rect x="838" y="1366.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="extra fancy">
					<rect x="741.6" y="1265.8" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="St. Anselm">
					<rect x="813.2" y="1257.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Pates Et Traditions">
					<rect x="881.2" y="1238" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Caracas Arepa Bar">
					<rect x="752.1" y="1329.3" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Fette Sau">
					<rect x="809.1" y="1290.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Manila Social Club">
					<rect x="757" y="1309.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="La Goulette">
					<rect x="595.5" y="1242.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Maison Premiere">
					<rect x="571.7" y="1269.3" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Aurora">
					<rect x="432.1" y="1191.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="egg">
					<rect x="570.9" y="1114.6" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Delaware and Hudson">
					<rect x="678.3" y="1090.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Baoburg">
					<rect x="673.4" y="1068.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="SEA Thai Restaurant">
					<rect x="650.4" y="1050.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Sweetwater Restaurant">
					<rect x="640.5" y="1005.8" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Cubana Socíal">
					<rect x="591.9" y="968.3" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="House of Small Wonder">
					<rect x="611.8" y="970.7" class="st2" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Rosarito Fish Shack">
					<rect x="618" y="935.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Cafe Mogador">
					<rect x="661.8" y="949.3" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Fat Goose">
					<rect x="679.8" y="927.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Miranda">
					<rect x="750.3" y="927.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Cafe Colette">
					<rect x="773.3" y="949" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Juniper">
					<rect x="697.4" y="998.2" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Dos Torros Taqueria">
					<rect x="748" y="1091.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="La Goulette">
					<rect x="752.8" y="1023.8" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Sweet Chick">
					<rect x="779.4" y="1009.9" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="El Almacén">
					<rect x="791.4" y="1115.9" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Oregano">
					<rect x="699.8" y="979.4" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Bakeri">
					<rect x="630.9" y="987.9" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Juliette Restaurant">
					<rect x="697.9" y="1107.1" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="La Nonna">
					<rect x="466.9" y="1024.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Sottocasa">
					<rect x="959.8" y="974.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="MP Taverna">
					<rect x="896.4" y="983.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Mable’s Smokehouse &amp; Banquet Hall">
					<rect x="776.2" y="845.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Reynard">
					<rect x="723.7" y="800.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Harvey at The Williamsburg Hotel">
					<rect x="703.4" y="821" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Smorgasburg">
					<rect x="554.4" y="836.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Paulie Gee’s">
					<rect x="713.9" y="374.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Greenpoint Beer and Ale">
					<rect x="788.2" y="614.5" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Halcyon Gourmet">
					<rect x="833.5" y="629.6" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Five Leaves">
					<rect x="1032.6" y="694.7" class="st3" width="15.2" height="15.2"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Luksus">
					<rect x="1056.9" y="732.1" class="st3" width="15.2" height="15.2"/>
				</g>
			</g>
			<g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Soloway">
					<circle class="st4" cx="919.9" cy="1615.8" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Asian Elephant Art &amp; Conservation Project">
					<circle class="st4" cx="1123.4" cy="1303.8" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Projekt722">
					<circle class="st4" cx="1322.8" cy="1284.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="City Reliquary Museum">
					<circle class="st4" cx="867.5" cy="1305.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="AG Gallery">
					<circle class="st4" cx="780.4" cy="1373.8" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Front Room Gallery">
					<circle class="st4" cx="774.1" cy="1287.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Williamsburg’s Sideshow Gallery">
					<circle class="st4" cx="572.7" cy="1357.3" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Williamsburg Art &amp; Historical Center">
					<circle class="st4" cx="503.3" cy="1496.9" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Easy Street Gallery">
					<circle class="st4" cx="586.1" cy="1244" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Journal Gallery">
					<circle class="st4" cx="532.5" cy="1203.4" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Art 101">
					<circle class="st4" cx="502.9" cy="1197.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Ventana244">
					<circle class="st4" cx="853.5" cy="1216.8" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Simon/Neuman² Gallery">
					<circle class="st4" cx="808.5" cy="1091.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Figureworks">
					<circle class="st4" cx="739.7" cy="1123.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Black &amp; White Gallery">
					<circle class="st4" cx="900.7" cy="1039" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="MOEITY">
					<circle class="st4" cx="863.1" cy="896.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="The Boiler | Pierogi">
					<circle class="st4" cx="874.4" cy="762.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Skink Ink Editions">
					<circle class="st4" cx="883.1" cy="989.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Northside Town Hall">
					<circle class="st4" cx="724" cy="1020.2" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Southfirst">
					<circle class="st4" cx="583.5" cy="964.8" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Brooklyn Oenology">
					<circle class="st4" cx="553.8" cy="1100.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="P339">
					<circle class="st4" cx="550.6" cy="1401.7" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Bunnycutlet Gallery">
					<circle class="st4" cx="742.6" cy="1291.5" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="17 Frost Street">
					<circle class="st4" cx="1048.5" cy="1074.3" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Brooklyn Art Library">
					<circle class="st4" cx="1053" cy="1101.6" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="REVERSE">
					<circle class="st4" cx="1074.1" cy="1099.1" r="7.6"/>
				</g>
				<g class="neighborhood-hotspots__marker" data-hotspot="Cinders Gallery">
					<circle class="st4" cx="1119.1" cy="1068.2" r="7.6"/>
				</g>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Baby’s All Right">
				<circle class="st5" cx="509" cy="1528.9" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Bembe">
				<circle class="st5" cx="437.3" cy="1472" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Miss Favela">
				<circle class="st5" cx="383.3" cy="1387.7" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="The Knitting Factory">
				<circle class="st5" cx="857.8" cy="1278.1" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="The Grand Victory">
				<circle class="st5" cx="721.1" cy="1317" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Ba’sik">
				<circle class="st5" cx="1348.1" cy="1288.6" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Warsaw">
				<circle class="st5" cx="1176.7" cy="799.7" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Muchmore’s">
				<circle class="st5" cx="969.4" cy="1143.3" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="National Sawdust">
				<circle class="st5" cx="597.3" cy="1006.6" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Music Hall of Williamsburg">
				<circle class="st5" cx="564" cy="981.1" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Rough Trade">
				<circle class="st5" cx="658.5" cy="896.2" r="7.6"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Gimme! Coffee">
				<rect x="1129.2" y="1424.5" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M1144.5,1439.9H1129v-15.5h15.5V1439.9z M1129.4,1439.5h14.8v-14.8h-14.8V1439.5z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Oslo Coffee Roasters">
				<rect x="538.6" y="1338.7" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M553.9,1354h-15.5v-15.5h15.5V1354z M538.8,1353.7h14.8v-14.8h-14.8V1353.7z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Sweatshop">
				<rect x="663.2" y="1242.2" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M678.6,1257.6H663V1242h15.5V1257.6z M663.4,1257.2h14.8v-14.8h-14.8V1257.2z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Lion’s Milk">
				<rect x="842.6" y="1163.7" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M857.9,1179.1h-15.5v-15.5h15.5V1179.1z M842.8,1178.7h14.8v-14.8h-14.8V1178.7z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Devocion">
				<rect x="435.4" y="1162.4" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M450.8,1177.7h-15.5v-15.5h15.5V1177.7z M435.6,1177.4h14.8v-14.8h-14.8V1177.4z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Blue Bottle Coffee">
				<rect x="622.4" y="1083.9" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M637.8,1099.2h-15.5v-15.5h15.5V1099.2z M622.6,1098.9h14.8v-14.8h-14.8V1098.9z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Sweetleaf">
				<rect x="532.5" y="951.5" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M547.9,966.9h-15.5v-15.5h15.5V966.9z M532.7,966.5h14.8v-14.8h-14.8V966.5z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Toby’s Estate Coffee">
				<rect x="693" y="1047.1" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M708.3,1062.4h-15.5v-15.5h15.5V1062.4z M693.1,1062.1h14.8v-14.8h-14.8V1062.1z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Schmackary’s">
				<rect x="732.1" y="1025.3" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M747.5,1040.6h-15.5v-15.5h15.5V1040.6z M732.3,1040.2h14.8v-14.8h-14.8V1040.2z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Vittoria Cafe">
				<rect x="762.4" y="998.2" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M777.7,1013.6h-15.5v-15.5h15.5V1013.6z M762.6,1013.2h14.8v-14.8h-14.8V1013.2z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Konditori">
				<rect x="783.8" y="1065.2" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M799.1,1080.6h-15.5V1065h15.5V1080.6z M784,1080.2h14.8v-14.8H784V1080.2z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Tea Bar">
				<rect x="845.9" y="1046.9" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M861.3,1062.3h-15.5v-15.5h15.5V1062.3z M846.1,1061.9h14.8v-14.8h-14.8V1061.9z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="The Bean">
				<rect x="883.1" y="929.3" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M898.4,944.7h-15.5v-15.5h15.5V944.7z M883.2,944.3H898v-14.8h-14.8V944.3z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Uro Cafe">
				<rect x="1152.5" y="766.8" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M1167.9,782.2h-15.5v-15.5h15.5V782.2z M1152.7,781.8h14.8V767h-14.8V781.8z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Budin">
				<rect x="998.8" y="604.1" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M1014.1,619.5h-15.5V604h15.5V619.5z M999,619.1h14.8v-14.8H999V619.1z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Upright Coffee">
				<rect x="937.4" y="370.5" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M952.8,385.9h-15.5v-15.5h15.5V385.9z M937.6,385.5h14.8v-14.8h-14.8V385.5z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Budin">
				<rect x="827.5" y="359.1" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M842.8,374.5h-15.5v-15.5h15.5V374.5z M827.6,374.1h14.8v-14.8h-14.8V374.1z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Grade Coffee">
				<rect x="697.9" y="941.8" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M713.3,957.1h-15.5v-15.5h15.5V957.1z M698.1,956.7h14.8v-14.8h-14.8V956.7z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="MatchaBar">
				<rect x="741.2" y="853.7" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M756.5,869.1H741v-15.5h15.5V869.1z M741.4,868.7h14.8v-14.8h-14.8V868.7z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Kinfolk 90">
				<rect x="720.2" y="836.1" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M735.6,851.4h-15.5v-15.5h15.5V851.4z M720.4,851.1h14.8v-14.8h-14.8V851.1z"/>
			</g>
			<g class="neighborhood-hotspots__marker" data-hotspot="Bakeri">
				<rect x="642.1" y="931.2" class="st6" width="15.2" height="15.2"/>
				<path class="st6" d="M657.4,946.5h-15.5V931h15.5V946.5z M642.2,946.1H657v-14.8h-14.8V946.1z"/>
			</g>
		</g>
	</g>
	<g>
		<path class="st0" d="M535,956.5l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H535V956.5z"/>
		<path class="st0" d="M537.6,962v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C538.2,963.8,537.6,963,537.6,962z"/>
		<path class="st0" d="M542.2,961.8v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2V957c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C542.7,963.8,542.2,962.9,542.2,961.8z"/>
	</g>
	<g>
		<path class="st0" d="M557.7,841.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V841.4z"/>
		<path class="st0" d="M560.2,846.6v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C560.6,848.6,560.2,847.7,560.2,846.6z"/>
		<path class="st0" d="M565.5,841.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V841.4z"/>
	</g>
	<g>
		<path class="st0" d="M559.5,983.8v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C560.1,985.6,559.5,984.9,559.5,983.8z"/>
		<path class="st0" d="M564.3,983.8v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C564.9,985.6,564.3,984.9,564.3,983.8z"/>
	</g>
	<g>
		<path class="st0" d="M579,967.5v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S579,968.7,579,967.5z
			 M581.7,967.5V966c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7C581.5,968.2,581.7,967.9,581.7,967.5z
			 M581.7,963.6v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C581.4,964.4,581.7,964,581.7,963.6z"
			/>
		<path class="st0" d="M586,967.1h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2H586V967.1z"/>
	</g>
	<g>
		<path class="st0" d="M549.5,1103.1v-4.9c0-1.3,0.9-1.9,2-1.9s1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C550.2,1104.9,549.5,1104.2,549.5,1103.1z M552.2,1103.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C552,1103.9,552.2,1103.5,552.2,1103.1z"/>
		<path class="st0" d="M554.3,1103.2v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C555,1104.9,554.3,1104.2,554.3,1103.2z"/>
	</g>
	<g>
		<path class="st0" d="M574.1,1124.3v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S574.1,1125.4,574.1,1124.3z
			 M576.7,1121v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C576.3,1121.7,576.7,1121.4,576.7,1121z"
			/>
		<path class="st0" d="M579,1124.3v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S579,1125.4,579,1124.3z M581.5,1121
			v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C581.2,1121.7,581.5,1121.4,581.5,1121z"/>
	</g>
	<g>
		<path class="st0" d="M590.6,1137v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C591.1,1138.9,590.6,1138.1,590.6,1137z"/>
		<path class="st0" d="M595.4,1137.2v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C596,1139,595.4,1138.2,595.4,1137.2z"/>
	</g>
	<g>
		<path class="st0" d="M607.5,928.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V928.8z"/>
		<path class="st0" d="M610.1,934.3v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C610.9,936,610.1,935.4,610.1,934.3z M612.7,934.3v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C612.5,935,612.7,934.7,612.7,934.3z M612.7,930.4V929c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7
			C612.5,931.1,612.7,930.8,612.7,930.4z"/>
	</g>
	<g>
		<path class="st0" d="M654.2,898.8v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C654.8,900.6,654.2,899.9,654.2,898.8z"/>
		<path class="st0" d="M661.3,893.1h-2.4V892h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C659.6,897.5,660.4,894.7,661.3,893.1z"/>
	</g>
	<g>
		<path class="st0" d="M635,953.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H635V953.9z"/>
		<path class="st0" d="M637.6,959.3v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C638.2,961.1,637.6,960.4,637.6,959.3z"/>
	</g>
	<g>
		<path class="st0" d="M665.3,959.4v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C666.1,961.1,665.3,960.4,665.3,959.4z M667.9,956.1v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C667.5,956.8,667.9,956.5,667.9,956.1z"/>
		<path class="st0" d="M672.3,959h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V959z"/>
	</g>
	<g>
		<path class="st0" d="M682.1,932.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V932.2z"/>
		<path class="st0" d="M684.7,937.6v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C685.6,939.4,684.7,938.8,684.7,937.6z M687.5,937.6v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C687.3,938.4,687.5,938,687.5,937.6z"/>
		<path class="st0" d="M689.7,938.5l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V938.5z"/>
	</g>
	<g>
		<path class="st0" d="M700.6,946.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V946.7z"/>
		<path class="st0" d="M705.4,951.8H703v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V951.8z"/>
		<path class="st0" d="M708,952.2v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C708.7,954,708,953.2,708,952.2z"/>
	</g>
	<g>
		<path class="st0" d="M764.7,1002.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1002.8z"/>
		<path class="st0" d="M767.3,1008.2v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C767.9,1010,767.3,1009.3,767.3,1008.2z"/>
		<path class="st0" d="M772,1008.3v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7C772.8,1010,772,1009.4,772,1008.3
			z M774.7,1008.3v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C774.5,1009,774.7,1008.7,774.7,1008.3z M774.7,1004.4v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C774.4,1005.1,774.7,1004.8,774.7,1004.4z"/>
	</g>
	<g>
		<path class="st0" d="M695.2,1052l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1052z"/>
		<path class="st0" d="M697.8,1057.5v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C698.4,1059.3,697.8,1058.6,697.8,1057.5z"/>
		<path class="st0" d="M702.5,1057.5v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C703.2,1059.3,702.5,1058.6,702.5,1057.5z"/>
	</g>
	<g>
		<path class="st0" d="M735.2,1030.5l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1030.5z"/>
		<path class="st0" d="M737.8,1035.9v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C738.4,1037.7,737.8,1037,737.8,1035.9z"/>
		<path class="st0" d="M743.2,1030.5l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1030.5z"/>
	</g>
	<g>
		<path class="st0" d="M782,1015l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H782V1015z"/>
		<path class="st0" d="M784.5,1020.3v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C785,1022.3,784.5,1021.4,784.5,1020.3z"/>
		<path class="st0" d="M789.2,1020.5v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C789.9,1022.3,789.2,1021.6,789.2,1020.5z"/>
	</g>
	<g>
		<path class="st0" d="M756,1029l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H756V1029z"/>
		<path class="st0" d="M759.3,1029l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1029z"/>
		<path class="st0" d="M761.9,1035.3l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1035.3z"/>
	</g>
	<g>
		<path class="st0" d="M701,1111.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H701V1111.9z"/>
		<path class="st0" d="M704.2,1111.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1111.9z"/>
		<path class="st0" d="M706.9,1117.3v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C707.7,1119.1,706.9,1118.5,706.9,1117.3z M709.6,1117.3v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C709.4,1118.1,709.6,1117.7,709.6,1117.3z"/>
	</g>
	<g>
		<path class="st0" d="M793.4,1121.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1121.1z"/>
		<path class="st0" d="M796.1,1126.5v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C796.9,1128.4,796.1,1127.7,796.1,1126.5z M798.8,1126.6v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C798.6,1127.3,798.8,1126.9,798.8,1126.6z"/>
		<path class="st0" d="M801,1126.5v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C801.9,1128.4,801,1127.7,801,1126.5z M803.8,1126.6v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C803.5,1127.3,803.8,1126.9,803.8,1126.6z"/>
	</g>
	<g>
		<path class="st0" d="M702.2,983.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V983.4z"/>
		<path class="st0" d="M704.8,989.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1V984c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V989.7z"/>
		<path class="st0" d="M709.2,988.7v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C709.7,990.6,709.2,989.8,709.2,988.7z"/>
	</g>
	<g>
		<path class="st0" d="M752.9,931.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V931.8z"/>
		<path class="st0" d="M755.5,938.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V938.1z"/>
		<path class="st0" d="M760,937.2v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8C760.9,939.1,760,938.5,760,937.2
			z M762.7,937.3v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7C762.5,938,762.7,937.6,762.7,937.3z
			"/>
	</g>
	<g>
		<path class="st0" d="M726.4,805.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V805.1z"/>
		<path class="st0" d="M729,811.4l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5H729v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1H729V811.4z"/>
		<path class="st0" d="M735.8,804.9h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C734.2,809.3,735,806.5,735.8,804.9z"/>
	</g>
	<g>
		<path class="st0" d="M705.9,825.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V825.9z"/>
		<path class="st0" d="M708.5,831.3v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C709.4,833.1,708.5,832.5,708.5,831.3z M711.2,831.3v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C711,832,711.2,831.7,711.2,831.3z"/>
		<path class="st0" d="M715.8,825.7h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C714.1,830,714.9,827.3,715.8,825.7z"/>
	</g>
	<g>
		<path class="st0" d="M703.2,848.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V848.1z"/>
		<path class="st0" d="M705.9,853.5v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C706.6,855.3,705.9,854.6,705.9,853.5z M708.5,853.5v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C708.3,854.3,708.5,853.9,708.5,853.5z"/>
		<path class="st0" d="M711.4,848.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V848.1z"/>
	</g>
	<g>
		<path class="st0" d="M721.4,891.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V891.6z"/>
		<path class="st0" d="M724,897v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C724.7,898.9,724,898.1,724,897z M726.7,897.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C726.5,897.8,726.7,897.4,726.7,897.1z"/>
		<path class="st0" d="M728.7,896.9v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C729.2,898.9,728.7,898,728.7,896.9z"/>
	</g>
	<g>
		<path class="st0" d="M722.6,840.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V840.7z"/>
		<path class="st0" d="M727.4,845.8H725v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V845.8z"/>
		<path class="st0" d="M730,846.1v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2V841c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C730.7,847.9,730,847.2,730,846.1z M732.7,846.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C732.5,846.9,732.7,846.5,732.7,846.1z"/>
	</g>
	<g>
		<path class="st0" d="M743.4,858.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V858.7z"/>
		<path class="st0" d="M748.2,863.8h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V863.8z"/>
		<path class="st0" d="M750.8,864.3v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8V862c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S750.8,865.3,750.8,864.3z
			 M753.4,860.9v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C753,861.7,753.4,861.4,753.4,860.9z"/>
	</g>
	<g>
		<path class="st0" d="M752.8,806.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V806.9z"/>
		<path class="st0" d="M755.4,812.4v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C756,814.2,755.4,813.5,755.4,812.4z"/>
		<path class="st0" d="M760.2,812.5v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S760.2,813.5,760.2,812.5z
			 M762.7,809.1v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C762.4,809.9,762.7,809.5,762.7,809.1z"
			/>
	</g>
	<g>
		<path class="st0" d="M807.1,802.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V802.7z"/>
		<path class="st0" d="M809.8,808v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2V803c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C810.5,809.9,809.8,809.2,809.8,808z M812.4,808.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C812.2,808.9,812.4,808.5,812.4,808.1z"/>
		<path class="st0" d="M814.6,808.1v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C815.5,809.9,814.6,809.3,814.6,808.1z M817.3,808.1V803c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7
			C817.1,808.8,817.3,808.5,817.3,808.1z"/>
	</g>
	<g>
		<path class="st0" d="M779.4,849.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V849.9z"/>
		<path class="st0" d="M782.6,849.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V849.9z"/>
		<path class="st0" d="M787.5,849.7h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C785.9,854.1,786.7,851.4,787.5,849.7z"/>
	</g>
	<g>
		<path class="st0" d="M701,1002.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H701V1002.8z"/>
		<path class="st0" d="M704.2,1002.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1002.8z"/>
		<path class="st0" d="M707.5,1002.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1002.8z"/>
	</g>
	<g>
		<path class="st0" d="M722.3,1017.4h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C720.6,1021.7,721.4,1019,722.3,1017.4z"/>
		<path class="st0" d="M724.4,1023v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C725,1024.8,724.4,1024.1,724.4,1023z"/>
	</g>
	<g>
		<path class="st0" d="M734.9,960.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1V955c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V960.7z"/>
		<path class="st0" d="M739.3,959.7v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C739.8,961.6,739.3,960.7,739.3,959.7z"/>
	</g>
	<g>
		<path class="st0" d="M730.9,1011.8l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1011.8z"/>
		<path class="st0" d="M735.5,1010.9v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C736.3,1012.7,735.5,1012.1,735.5,1010.9z M738.2,1010.9v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C738,1011.6,738.2,1011.3,738.2,1010.9z"/>
	</g>
	<g>
		<path class="st0" d="M776.7,959.4v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C777.4,961.1,776.7,960.4,776.7,959.4z M779.3,956.1v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C778.9,956.8,779.3,956.5,779.3,956.1z"/>
		<path class="st0" d="M781.4,959.1v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C781.9,961.1,781.4,960.2,781.4,959.1z"/>
	</g>
	<g>
		<path class="st0" d="M676.5,1078.2v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C677.4,1079.9,676.5,1079.4,676.5,1078.2z M679.2,1078.2v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C679,1078.9,679.2,1078.6,679.2,1078.2z M679.2,1074.3v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6
			v1.4c0,0.4,0.3,0.7,0.7,0.7C678.9,1075,679.2,1074.7,679.2,1074.3z"/>
		<path class="st0" d="M681.4,1078.2v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S681.4,1079.3,681.4,1078.2z
			 M683.9,1074.9v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C683.6,1075.6,683.9,1075.3,683.9,1074.9z"/>
	</g>
	<g>
		<path class="st0" d="M734.4,1082.6v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C734.9,1084.6,734.4,1083.7,734.4,1082.6z"/>
		<path class="st0" d="M739.2,1082.9v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S739.2,1083.9,739.2,1082.9z
			 M741.8,1079.5v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C741.4,1080.3,741.8,1080,741.8,1079.5
			z"/>
	</g>
	<g>
		<path class="st0" d="M804.7,1094.2v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C805.5,1096,804.7,1095.4,804.7,1094.2z M807.4,1094.2v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C807.1,1094.9,807.4,1094.6,807.4,1094.2z M807.4,1090.3v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C807.1,1091.1,807.4,1090.7,807.4,1090.3z"/>
		<path class="st0" d="M810.2,1088.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1088.7z"/>
	</g>
	<g>
		<path class="st0" d="M826.1,1087.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1087.1z"/>
	</g>
	<g>
		<path class="st0" d="M879.6,1098.3l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1098.3z"/>
		<path class="st0" d="M882.2,1103.7v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C883.1,1105.5,882.2,1104.9,882.2,1103.7z M884.9,1103.7v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C884.7,1104.5,884.9,1104.1,884.9,1103.7z"/>
	</g>
	<g>
		<path class="st0" d="M876.2,1022l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1022z"/>
		<path class="st0" d="M881.1,1027.1h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1027.1z"/>
	</g>
	<g>
		<path class="st0" d="M896.3,1041.6v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C897,1043.5,896.3,1042.7,896.3,1041.6z M898.9,1041.7v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C898.7,1042.4,898.9,1042,898.9,1041.7z"/>
		<path class="st0" d="M901,1041.5v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5H901v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C901.5,1043.5,901,1042.6,901,1041.5z"/>
	</g>
	<g>
		<path class="st0" d="M878.9,991.9v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C879.7,993.6,878.9,993,878.9,991.9z M881.5,991.9v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C881.3,992.6,881.5,992.3,881.5,991.9z M881.5,988v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7
			C881.3,988.7,881.5,988.3,881.5,988z"/>
		<path class="st0" d="M883.7,992.6l1.3-2.4c0.6-1.2,1-2.2,1-3.1V987c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V992.6z"/>
	</g>
	<g>
		<path class="st0" d="M862.9,924.9l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V924.9z"/>
		<path class="st0" d="M868,918.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H868V918.6z"/>
	</g>
	<g>
		<path class="st0" d="M809.7,875.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V875.4z"/>
		<path class="st0" d="M812.2,880.7v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C812.7,882.7,812.2,881.8,812.2,880.7z"/>
	</g>
	<g>
		<path class="st0" d="M788.1,839.3v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8V837c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S788.1,840.3,788.1,839.3z
			 M790.6,835.9v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C790.3,836.7,790.6,836.4,790.6,835.9z"
			/>
	</g>
	<g>
		<path class="st0" d="M774.4,828.2v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2V823c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C775.2,829.9,774.4,829.4,774.4,828.2z M777,828.2v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C776.8,828.9,777,828.6,777,828.2z M777,824.3v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7
			C776.8,825,777,824.7,777,824.3z"/>
	</g>
	<g>
		<path class="st0" d="M872.2,801.7V801h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C872.9,803.5,872.2,802.7,872.2,801.7z"/>
	</g>
	<g>
		<path class="st0" d="M795,869.7h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2H795V869.7z"/>
		<path class="st0" d="M799.9,864.4h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C798.2,868.8,799,866,799.9,864.4z"/>
	</g>
	<g>
		<path class="st0" d="M742.7,826.2l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V826.2z"/>
		<path class="st0" d="M749.4,825H747v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V825z"/>
	</g>
	<g>
		<path class="st0" d="M685.9,797.9l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V797.9z"/>
		<path class="st0" d="M690.4,797.1v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C691.1,798.8,690.4,798.1,690.4,797.1z"/>
	</g>
	<g>
		<path class="st0" d="M861.3,893.3h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C859.7,897.7,860.5,894.9,861.3,893.3z"/>
		<path class="st0" d="M865.1,898.6h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2V896l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V898.6z"/>
	</g>
	<g>
		<path class="st0" d="M872.7,760h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5H871C871,764.4,871.8,761.6,872.7,760z"/>
		<path class="st0" d="M877.1,760h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C875.4,764.4,876.2,761.6,877.1,760z"/>
	</g>
	<g>
		<path class="st0" d="M1034.9,698.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V698.9z"/>
		<path class="st0" d="M1037.5,704.3v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1038.4,706.1,1037.5,705.5,1037.5,704.3z M1040.2,704.3v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1040,705,1040.2,704.7,1040.2,704.3z"/>
		<path class="st0" d="M1044.7,704h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V704z"/>
	</g>
	<g>
		<path class="st0" d="M1001.2,608.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V608.7z"/>
		<path class="st0" d="M1006,613.8h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V613.8z"/>
		<path class="st0" d="M1008.6,615.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V615.1z"/>
	</g>
	<g>
		<path class="st0" d="M1060.3,736.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V736.7z"/>
		<path class="st0" d="M1063.6,736.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V736.7z"/>
		<path class="st0" d="M1066.2,742.1v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C1066.8,743.9,1066.2,743.2,1066.2,742.1z"/>
	</g>
	<g>
		<path class="st0" d="M1155,771.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V771.8z"/>
		<path class="st0" d="M1157.6,777.3v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C1158.3,779.1,1157.6,778.3,1157.6,777.3z"/>
		<path class="st0" d="M1164.7,771.6h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1163.1,776,1163.9,773.2,1164.7,771.6z"/>
	</g>
	<g>
		<path class="st0" d="M1088,733.6h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2V731l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V733.6z"/>
		<path class="st0" d="M1090.6,733.9v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C1091.3,735.7,1090.6,735,1090.6,733.9z"/>
	</g>
	<g>
		<path class="st0" d="M1095.2,751.6v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C1095.7,753.6,1095.2,752.7,1095.2,751.6z"/>
		<path class="st0" d="M1099.9,751.8v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1100.8,753.6,1099.9,753,1099.9,751.8z M1102.7,751.8v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1102.5,752.5,1102.7,752.2,1102.7,751.8z"/>
	</g>
	<g>
		<path class="st0" d="M1172.5,802.2v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C1173.1,804,1172.5,803.3,1172.5,802.2z"/>
		<path class="st0" d="M1177.3,802.3v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8V800c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S1177.3,803.3,1177.3,802.3z
			 M1179.9,798.9v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C1179.5,799.7,1179.9,799.4,1179.9,798.9z"/>
	</g>
	<g>
		<path class="st0" d="M1115.9,795.2v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C1116.4,797.2,1115.9,796.3,1115.9,795.2z"/>
	</g>
	<g>
		<path class="st0" d="M1076.4,705.4l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V705.4z"/>
		<path class="st0" d="M1083.2,698.9h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1081.6,703.3,1082.4,700.5,1083.2,698.9z"/>
	</g>
	<g>
		<path class="st0" d="M835.8,634.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V634.4z"/>
		<path class="st0" d="M838.5,639.8v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C839.3,641.6,838.5,641,838.5,639.8z M841.2,639.8v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7
			C841,640.5,841.2,640.2,841.2,639.8z"/>
		<path class="st0" d="M843.4,639.7v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C844.2,641.6,843.4,640.9,843.4,639.7z M846.1,639.8v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C845.9,640.5,846.1,640.2,846.1,639.8z"/>
	</g>
	<g>
		<path class="st0" d="M790.5,618.5l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V618.5z"/>
		<path class="st0" d="M793.1,623.9v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C794,625.8,793.1,625.2,793.1,623.9z M795.8,624v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7
			C795.6,624.7,795.8,624.3,795.8,624z"/>
		<path class="st0" d="M798,624v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C798.7,625.8,798,625.1,798,624z"/>
	</g>
	<g>
		<path class="st0" d="M774,632v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3H775V629h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5H774v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C774.4,633.9,774,633.1,774,632z"/>
		<path class="st0" d="M779.3,626.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V626.7z"/>
	</g>
	<g>
		<path class="st0" d="M770.4,537.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V537.2z"/>
		<path class="st0" d="M773.1,542.6v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C773.8,544.4,773.1,543.7,773.1,542.6z M775.7,542.6v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C775.5,543.4,775.7,543,775.7,542.6z"/>
	</g>
	<g>
		<path class="st0" d="M716.3,379.3l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V379.3z"/>
		<path class="st0" d="M718.9,385.6l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V385.6z"/>
		<path class="st0" d="M723.4,384.8v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C724,386.6,723.4,385.8,723.4,384.8z"/>
	</g>
	<g>
		<path class="st0" d="M830.5,364.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V364.1z"/>
		<path class="st0" d="M835.3,369.2h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V369.2z"/>
		<path class="st0" d="M838.6,364.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V364.1z"/>
	</g>
	<g>
		<path class="st0" d="M939.9,375.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V375.6z"/>
		<path class="st0" d="M942.5,381.1v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C943.1,382.9,942.5,382.1,942.5,381.1z"/>
		<path class="st0" d="M947.3,381v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C948,382.9,947.3,382.1,947.3,381z M949.9,381.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C949.7,381.8,949.9,381.4,949.9,381.1z"/>
	</g>
	<g>
		<path class="st0" d="M747.9,336.9v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3H749v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C748.4,338.9,747.9,338,747.9,336.9z"/>
		<path class="st0" d="M752.6,337.1v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C753.2,338.9,752.6,338.2,752.6,337.1z"/>
	</g>
	<g>
		<path class="st0" d="M810.7,335.4h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5H809C809,339.7,809.8,337,810.7,335.4z"/>
	</g>
	<g>
		<path class="st0" d="M899.4,988.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V988.1z"/>
		<path class="st0" d="M902.6,988.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V988.1z"/>
		<path class="st0" d="M905.2,993.5v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C906,995.4,905.2,994.6,905.2,993.5z M907.9,993.6v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C907.7,994.3,907.9,993.9,907.9,993.6z"/>
	</g>
	<g>
		<path class="st0" d="M962.2,979.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V979.2z"/>
		<path class="st0" d="M964.7,984.4V984h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C965.2,986.4,964.7,985.5,964.7,984.4z"/>
		<path class="st0" d="M969.4,985.5l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V985.5z"/>
	</g>
	<g>
		<path class="st0" d="M989.5,1075l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1075z"/>
		<path class="st0" d="M992.1,1081.3l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5H992v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1081.3z"/>
		<path class="st0" d="M996.6,1080.4v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C997.3,1082.2,996.6,1081.5,996.6,1080.4z M999.2,1080.4v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C999.1,1081.2,999.2,1080.8,999.2,1080.4z"/>
	</g>
	<g>
		<path class="st0" d="M1022.6,1101.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1101.2z"/>
		<path class="st0" d="M1025.9,1101.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1101.2z"/>
		<path class="st0" d="M1030.7,1106.3h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1106.3z"/>
	</g>
	<g>
		<path class="st0" d="M1014.1,1034l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1034z"/>
		<path class="st0" d="M1016.6,1040.3l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1040.3z"/>
		<path class="st0" d="M1021.2,1039.5v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C1021.9,1041.2,1021.2,1040.6,1021.2,1039.5z M1023.7,1036.2v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2
			c0,0.4,0.3,0.7,0.6,0.7C1023.4,1037,1023.7,1036.6,1023.7,1036.2z"/>
	</g>
	<g>
		<path class="st0" d="M1017.3,1061.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1061.7z"/>
		<path class="st0" d="M1019.9,1067.1v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1020.8,1068.9,1019.9,1068.3,1019.9,1067.1z M1022.6,1067.1v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1022.4,1067.9,1022.6,1067.5,1022.6,1067.1z"/>
		<path class="st0" d="M1024.9,1067.2v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S1024.9,1068.3,1024.9,1067.2z
			 M1027.5,1063.9v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C1027.1,1064.6,1027.5,1064.3,1027.5,1063.9z"/>
	</g>
	<g>
		<path class="st0" d="M987.4,1057v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C987.8,1058.9,987.4,1058,987.4,1057z"/>
		<path class="st0" d="M994.4,1051.5H992v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C992.7,1055.9,993.5,1053.1,994.4,1051.5z"/>
	</g>
	<g>
		<path class="st0" d="M965.1,1145.9v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C965.8,1147.7,965.1,1147,965.1,1145.9z"/>
		<path class="st0" d="M972.1,1145.5h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1145.5z"/>
	</g>
	<g>
		<path class="st0" d="M1035.6,1161l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1161z"/>
		<path class="st0" d="M1040.5,1160.9h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1038.9,1165.2,1039.7,1162.5,1040.5,1160.9z"/>
	</g>
	<g>
		<path class="st0" d="M1048.4,1104.3v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C1049.2,1106.2,1048.4,1105.4,1048.4,1104.3z M1051.1,1104.4v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C1050.9,1105.1,1051.1,1104.7,1051.1,1104.4z"/>
		<path class="st0" d="M1055.4,1104h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1104z"/>
	</g>
	<g>
		<path class="st0" d="M1050.6,1226h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1226z"/>
		<path class="st0" d="M1053.2,1226.3v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C1054,1228.2,1053.2,1227.4,1053.2,1226.3z M1055.9,1226.4v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C1055.7,1227.1,1055.9,1226.7,1055.9,1226.4z"/>
	</g>
	<g>
		<path class="st0" d="M1048.6,1277.4h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1277.4z"/>
		<path class="st0" d="M1053.4,1277.4h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1277.4z"/>
	</g>
	<g>
		<path class="st0" d="M980,1307.4v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5H980v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C980.5,1309.4,980,1308.5,980,1307.4z"/>
		<path class="st0" d="M984.7,1307.7v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C985.5,1309.4,984.7,1308.8,984.7,1307.7z M987.3,1307.7v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C987.1,1308.4,987.3,1308.1,987.3,1307.7z M987.3,1303.8v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C987.1,1304.5,987.3,1304.2,987.3,1303.8z"/>
	</g>
	<g>
		<path class="st0" d="M883.6,1242.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1242.9z"/>
		<path class="st0" d="M886.2,1249.2l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1249.2z"/>
		<path class="st0" d="M892.9,1248h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1H894v2h-1.2V1248z"/>
	</g>
	<g>
		<path class="st0" d="M844.8,1168.3l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1168.3z"/>
		<path class="st0" d="M849.6,1173.4h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1173.4z"/>
		<path class="st0" d="M852.2,1173.9v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C853,1175.6,852.2,1175,852.2,1173.9z M854.9,1173.9v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C854.6,1174.5,854.9,1174.3,854.9,1173.9z M854.8,1170v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6
			v1.4c0,0.4,0.3,0.7,0.7,0.7C854.6,1170.7,854.8,1170.3,854.8,1170z"/>
	</g>
	<g>
		<path class="st0" d="M849.2,1219.4v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S849.2,1220.5,849.2,1219.4z
			 M851.9,1219.4v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C851.7,1220.1,851.9,1219.8,851.9,1219.4z M851.9,1215.5v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C851.6,1216.2,851.9,1215.9,851.9,1215.5z"/>
		<path class="st0" d="M854.1,1219.3v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C854.7,1221.1,854.1,1220.4,854.1,1219.3z"/>
	</g>
	<g>
		<path class="st0" d="M853.7,1280.9v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C854.3,1282.7,853.7,1281.9,853.7,1280.9z"/>
		<path class="st0" d="M858.4,1281.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1281.7z"/>
	</g>
	<g>
		<path class="st0" d="M863.2,1307.5v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C864,1309.4,863.2,1308.7,863.2,1307.5z M865.9,1307.6v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C865.7,1308.3,865.9,1308,865.9,1307.6z"/>
		<path class="st0" d="M868,1307.7v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C868.8,1309.4,868,1308.8,868,1307.7z M870.7,1307.7v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C870.5,1308.4,870.7,1308.1,870.7,1307.7z M870.7,1303.8v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C870.4,1304.5,870.7,1304.2,870.7,1303.8z"/>
	</g>
	<g>
		<path class="st0" d="M836.4,1276.7h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1276.7z"/>
		<path class="st0" d="M838.9,1276.9v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3H840v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5H839v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C839.4,1278.9,838.9,1278,838.9,1276.9z"/>
	</g>
	<g>
		<path class="st0" d="M845.5,1302.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1302.6z"/>
		<path class="st0" d="M848.1,1308.9l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5H848v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1308.9z"/>
	</g>
	<g>
		<path class="st0" d="M815.9,1262.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1262.2z"/>
		<path class="st0" d="M818.4,1267.4v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C818.9,1269.4,818.4,1268.5,818.4,1267.4z"/>
		<path class="st0" d="M823,1267.4v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C823.5,1269.4,823,1268.5,823,1267.4z"/>
	</g>
	<g>
		<path class="st0" d="M811.4,1295.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1295.7z"/>
		<path class="st0" d="M814,1301.1v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C814.9,1302.9,814,1302.3,814,1301.1z M816.7,1301.1v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C816.5,1301.8,816.7,1301.5,816.7,1301.1z"/>
		<path class="st0" d="M818.9,1300.9v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C819.3,1302.9,818.9,1302,818.9,1300.9z"/>
	</g>
	<g>
		<path class="st0" d="M760,1314.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H760V1314.4z"/>
		<path class="st0" d="M763.3,1314.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1314.4z"/>
		<path class="st0" d="M766,1320v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S766,1321,766,1320z M768.5,1316.6
			v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C768.1,1317.4,768.5,1317,768.5,1316.6z"/>
	</g>
	<g>
		<path class="st0" d="M744.3,1270.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1270.6z"/>
		<path class="st0" d="M747,1276v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8C747.8,1277.8,747,1277.2,747,1276
			z M749.7,1276v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7
			C749.5,1276.7,749.7,1276.4,749.7,1276z"/>
		<path class="st0" d="M752.6,1270.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1270.6z"/>
	</g>
	<g>
		<path class="st0" d="M665.9,1247.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1247.4z"/>
		<path class="st0" d="M668.5,1252.8v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C669.2,1254.6,668.5,1253.9,668.5,1252.8z"/>
		<path class="st0" d="M673.3,1253.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1253.7z"/>
	</g>
	<g>
		<path class="st0" d="M598.9,1246.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1246.6z"/>
		<path class="st0" d="M602.2,1246.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1246.6z"/>
		<path class="st0" d="M604.8,1252.9l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1252.9z"/>
	</g>
	<g>
		<path class="st0" d="M581.9,1246.5v-4.9c0-1.3,0.9-1.9,2-1.9s1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C582.6,1248.4,581.9,1247.6,581.9,1246.5z M584.5,1246.6v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C584.3,1247.3,584.5,1246.9,584.5,1246.6z"/>
		<path class="st0" d="M586.8,1246.7v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C587.5,1248.4,586.8,1247.7,586.8,1246.7z M589.3,1243.3v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2
			c0,0.4,0.3,0.7,0.6,0.7C588.9,1244.1,589.3,1243.8,589.3,1243.3z"/>
	</g>
	<g>
		<path class="st0" d="M498.4,1199.6v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7H501v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C499.1,1201.5,498.4,1200.8,498.4,1199.6z M501,1199.7v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C500.9,1200.4,501,1200.1,501,1199.7z"/>
		<path class="st0" d="M503.2,1200.6l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1200.6z"/>
	</g>
	<g>
		<path class="st0" d="M435.9,1201.8v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C436.7,1203.5,435.9,1202.9,435.9,1201.8z M438.5,1201.8v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C438.3,1202.5,438.5,1202.2,438.5,1201.8z M438.5,1197.9v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C438.3,1198.6,438.5,1198.3,438.5,1197.9z"/>
		<path class="st0" d="M443,1196.1h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C441.4,1200.5,442.2,1197.7,443,1196.1z"/>
	</g>
	<g>
		<path class="st0" d="M419.5,1143.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1143.1z"/>
		<path class="st0" d="M424,1143.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1H424V1143.1z"/>
	</g>
	<g>
		<path class="st0" d="M437.7,1166.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1166.9z"/>
		<path class="st0" d="M442.5,1172h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1172z"/>
		<path class="st0" d="M445,1172.2v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C445.5,1174.2,445,1173.3,445,1172.2z"/>
	</g>
	<g>
		<path class="st0" d="M531.1,1200.3h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C529.4,1204.7,530.2,1201.9,531.1,1200.3z"/>
		<path class="st0" d="M533.1,1205.8v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C533.6,1207.7,533.1,1206.8,533.1,1205.8z"/>
	</g>
	<g>
		<path class="st0" d="M536.2,1180.5l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1180.5z"/>
		<path class="st0" d="M540.7,1179.8v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C541.4,1181.5,540.7,1180.8,540.7,1179.8z M543.3,1176.4v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2
			c0,0.4,0.3,0.7,0.6,0.7C542.9,1177.2,543.3,1176.9,543.3,1176.4z"/>
	</g>
	<g>
		<path class="st0" d="M576.4,1162.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1162.7z"/>
		<path class="st0" d="M580.9,1161.8v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C581.6,1163.6,580.9,1162.9,580.9,1161.8z M583.6,1161.8v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C583.4,1162.6,583.6,1162.2,583.6,1161.8z"/>
	</g>
	<g>
		<path class="st0" d="M556.4,1188.3h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1188.3z"/>
		<path class="st0" d="M559.7,1183.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1183.2z"/>
	</g>
	<g>
		<path class="st0" d="M574.9,1274.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1274.1z"/>
		<path class="st0" d="M578.2,1274.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1274.1z"/>
		<path class="st0" d="M580.8,1279.7v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C581.6,1281.4,580.8,1280.8,580.8,1279.7z M583.4,1279.7v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C583.2,1280.4,583.4,1280.1,583.4,1279.7z M583.4,1275.8v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C583.2,1276.5,583.4,1276.1,583.4,1275.8z"/>
	</g>
	<g>
		<path class="st0" d="M540.9,1343.2l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1343.2z"/>
		<path class="st0" d="M543.5,1348.7v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C544.1,1350.5,543.5,1349.8,543.5,1348.7z"/>
		<path class="st0" d="M548.3,1348.6v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C549.2,1350.5,548.3,1349.9,548.3,1348.6z M551,1348.7v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C550.8,1349.4,551,1349,551,1348.7z"/>
	</g>
	<g>
		<path class="st0" d="M568,1360v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7C568.8,1361.7,568,1361.1,568,1360
			z M570.7,1360v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C570.5,1360.7,570.7,1360.4,570.7,1360z M570.7,1356.1v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C570.4,1356.8,570.7,1356.4,570.7,1356.1z"/>
		<path class="st0" d="M572.9,1359.8v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C573.7,1361.7,572.9,1361.1,572.9,1359.8z M575.6,1359.9v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C575.4,1360.6,575.6,1360.2,575.6,1359.9z"/>
	</g>
	<g>
		<path class="st0" d="M549,1398.6h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C547.3,1402.9,548.1,1400.2,549,1398.6z"/>
		<path class="st0" d="M551.1,1404.1v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C551.8,1406,551.1,1405.2,551.1,1404.1z M553.7,1404.2v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C553.6,1404.9,553.7,1404.6,553.7,1404.2z"/>
	</g>
	<g>
		<path class="st0" d="M504.3,1531.7v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C504.9,1533.5,504.3,1532.8,504.3,1531.7z"/>
		<path class="st0" d="M509,1531.8v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C509.8,1533.5,509,1532.9,509,1531.8z M511.7,1531.8v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C511.5,1532.5,511.7,1532.2,511.7,1531.8z M511.7,1527.9v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C511.4,1528.6,511.7,1528.2,511.7,1527.9z"/>
	</g>
	<g>
		<path class="st0" d="M498.8,1499.7v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C499.6,1501.4,498.8,1500.8,498.8,1499.7z M501.5,1499.7v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C501.2,1500.4,501.5,1500.1,501.5,1499.7z M501.4,1495.8v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C501.2,1496.5,501.4,1496.2,501.4,1495.8z"/>
		<path class="st0" d="M503.6,1499.6v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C504.4,1501.4,503.6,1500.7,503.6,1499.6z M506.3,1499.6v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C506.1,1500.4,506.3,1500,506.3,1499.6z"/>
	</g>
	<g>
		<path class="st0" d="M433.1,1474.8v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C433.7,1476.6,433.1,1475.9,433.1,1474.8z"/>
		<path class="st0" d="M437.9,1474.7v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C438.8,1476.6,437.9,1476,437.9,1474.7z M440.6,1474.8v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C440.4,1475.5,440.6,1475.1,440.6,1474.8z"/>
	</g>
	<g>
		<path class="st0" d="M379.3,1390.3v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C379.9,1392,379.3,1391.3,379.3,1390.3z"/>
		<path class="st0" d="M383.9,1390.1v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3H385v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5H384v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C384.4,1392,383.9,1391.1,383.9,1390.1z"/>
	</g>
	<g>
		<path class="st0" d="M772.1,1284.2h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C770.4,1288.6,771.2,1285.8,772.1,1284.2z"/>
		<path class="st0" d="M774.2,1290.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1290.7z"/>
	</g>
	<g>
		<path class="st0" d="M840.3,1370.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1370.8z"/>
		<path class="st0" d="M842.8,1376.1v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C843.2,1378.1,842.8,1377.2,842.8,1376.1z"/>
		<path class="st0" d="M849.7,1375.9h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1375.9z"/>
	</g>
	<g>
		<path class="st0" d="M776.7,1376.2v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C777.4,1378.1,776.7,1377.3,776.7,1376.2z M779.3,1376.3v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C779.2,1377,779.3,1376.6,779.3,1376.3z"/>
		<path class="st0" d="M782.2,1370.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1370.8z"/>
	</g>
	<g>
		<path class="st0" d="M774.6,1341.5l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1341.5z"/>
		<path class="st0" d="M777.2,1347v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C777.9,1348.7,777.2,1348.1,777.2,1347z M779.8,1343.7v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2
			c0,0.4,0.3,0.7,0.6,0.7C779.4,1344.4,779.8,1344.1,779.8,1343.7z"/>
	</g>
	<g>
		<path class="st0" d="M755.7,1339.7v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S755.7,1340.7,755.7,1339.7z
			 M758.3,1336.3v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C757.9,1337.1,758.3,1336.8,758.3,1336.3z"/>
		<path class="st0" d="M760.5,1339.6v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C761.1,1341.4,760.5,1340.7,760.5,1339.6z"/>
	</g>
	<g>
		<path class="st0" d="M738.1,1294v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C738.8,1295.9,738.1,1295.1,738.1,1294z M740.7,1294.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C740.6,1294.8,740.7,1294.4,740.7,1294.1z"/>
		<path class="st0" d="M742.9,1294v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C743.7,1295.9,742.9,1295.1,742.9,1294z M745.6,1294.1v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C745.4,1294.8,745.6,1294.4,745.6,1294.1z"/>
	</g>
	<g>
		<path class="st0" d="M733.8,1240.9h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1240.9z"/>
		<path class="st0" d="M736.4,1242.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1242.1z"/>
	</g>
	<g>
		<path class="st0" d="M717.3,1319.9v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C718,1321.7,717.3,1320.9,717.3,1319.9z"/>
		<path class="st0" d="M722.8,1314.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1314.4z"/>
	</g>
	<g>
		<path class="st0" d="M1138.3,1264.6v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C1139,1266.3,1138.3,1265.7,1138.3,1264.6z M1140.8,1261.3v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2
			c0,0.4,0.3,0.7,0.6,0.7C1140.4,1262.1,1140.8,1261.7,1140.8,1261.3z"/>
		<path class="st0" d="M1143,1265.4l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1265.4z"/>
	</g>
	<g>
		<path class="st0" d="M1121.6,1300.6h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1119.9,1305,1120.7,1302.3,1121.6,1300.6z"/>
		<path class="st0" d="M1123.7,1306.2v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1124.6,1308.1,1123.7,1307.5,1123.7,1306.2z M1126.4,1306.3v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1126.2,1307,1126.4,1306.6,1126.4,1306.3z"/>
	</g>
	<g>
		<path class="st0" d="M915.4,1618.3v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S915.4,1619.4,915.4,1618.3z
			 M918.1,1618.3v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C917.9,1619,918.1,1618.7,918.1,1618.3z M918.1,1614.4v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C917.8,1615.1,918.1,1614.8,918.1,1614.4z"/>
		<path class="st0" d="M920.1,1618v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C920.6,1620,920.1,1619.1,920.1,1618z"/>
	</g>
	<g>
		<path class="st0" d="M1321,1281.3h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1319.4,1285.7,1320.2,1282.9,1321,1281.3z"/>
		<path class="st0" d="M1323.1,1287v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C1323.9,1288.8,1323.1,1288.2,1323.1,1287z M1325.7,1287v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C1325.5,1287.7,1325.7,1287.4,1325.7,1287z M1325.7,1283.1v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C1325.5,1283.8,1325.7,1283.5,1325.7,1283.1z"/>
	</g>
	<g>
		<path class="st0" d="M1345.9,1290.9h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1290.9z"/>
		<path class="st0" d="M1348.6,1291.4v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2
			C1349.3,1293.1,1348.6,1292.4,1348.6,1291.4z M1351.1,1288v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2
			c0,0.4,0.3,0.7,0.6,0.7C1350.8,1288.8,1351.1,1288.4,1351.1,1288z"/>
	</g>
	<g>
		<path class="st0" d="M1154.2,1308.5l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1308.5z"/>
	</g>
	<g>
		<path class="st0" d="M1059.7,1417.3h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1417.3z"/>
	</g>
	<g>
		<path class="st0" d="M1158.2,1384.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1384.8z"/>
		<path class="st0" d="M1160.8,1391.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1391.1z"/>
		<path class="st0" d="M1165.3,1391.1l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1391.1z"/>
	</g>
	<g>
		<path class="st0" d="M1131.4,1429.5l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1429.5z"/>
		<path class="st0" d="M1136.2,1434.6h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1434.6z"/>
		<path class="st0" d="M1141.1,1434.6h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1434.6z"/>
	</g>
	<g>
		<path class="st0" d="M1079.9,1031.7v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S1079.9,1032.8,1079.9,1031.7z
			 M1082.4,1028.4v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C1082.1,1029.1,1082.4,1028.8,1082.4,1028.4z"/>
		<path class="st0" d="M1084.7,1031.6v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1085.6,1033.4,1084.7,1032.8,1084.7,1031.6z M1087.4,1031.6v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1087.2,1032.4,1087.4,1032,1087.4,1031.6z"/>
	</g>
	<g>
		<path class="st0" d="M1044.1,1076.7v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C1044.8,1078.6,1044.1,1077.9,1044.1,1076.7z M1046.7,1076.8v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C1046.6,1077.5,1046.7,1077.2,1046.7,1076.8z"/>
		<path class="st0" d="M1048.9,1076.8v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1049.8,1078.6,1048.9,1078,1048.9,1076.8z M1051.7,1076.8v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1051.5,1077.5,1051.7,1077.2,1051.7,1076.8z"/>
	</g>
	<g>
		<path class="st0" d="M1114.6,1070.9v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C1115.4,1072.8,1114.6,1072,1114.6,1070.9z M1117.3,1071v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C1117.1,1071.7,1117.3,1071.3,1117.3,1071z"/>
		<path class="st0" d="M1121.8,1065.3h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1120.1,1069.7,1120.9,1066.9,1121.8,1065.3z"/>
	</g>
	<g>
		<path class="st0" d="M1132.3,1093.8v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S1132.3,1095,1132.3,1093.8z
			 M1135,1093.8v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C1134.8,1094.5,1135,1094.2,1135,1093.8z M1135,1089.9v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C1134.7,1090.6,1135,1090.3,1135,1089.9z"/>
		<path class="st0" d="M1137.1,1093.8v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S1137.1,1095,1137.1,1093.8z
			 M1139.8,1093.8v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C1139.6,1094.5,1139.8,1094.2,1139.8,1093.8z M1139.8,1089.9v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C1139.5,1090.6,1139.8,1090.3,1139.8,1089.9z"/>
	</g>
	<g>
		<path class="st0" d="M1325.6,1009.2v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C1326.1,1011.2,1325.6,1010.3,1325.6,1009.2z"/>
		<path class="st0" d="M1330.3,1009.3v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C1331.1,1011.2,1330.3,1010.4,1330.3,1009.3z M1333,1009.4v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C1332.8,1010.1,1333,1009.7,1333,1009.4z"/>
	</g>
	<g>
		<path class="st0" d="M1316.9,1080.5v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S1316.9,1081.6,1316.9,1080.5z
			 M1319.4,1077.2v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C1319,1077.9,1319.4,1077.6,1319.4,1077.2z"/>
		<path class="st0" d="M1322.3,1075l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1075z"/>
	</g>
	<g>
		<path class="st0" d="M1096.7,1122l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1122z"/>
		<path class="st0" d="M1101.1,1121.2v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S1101.1,1122.3,1101.1,1121.2z
			 M1103.8,1121.2v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C1103.6,1121.9,1103.8,1121.6,1103.8,1121.2z M1103.8,1117.3v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C1103.5,1118,1103.8,1117.7,1103.8,1117.3z"/>
	</g>
	<g>
		<path class="st0" d="M1089.1,1059.2v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C1089.6,1061.2,1089.1,1060.3,1089.1,1059.2z"/>
		<path class="st0" d="M1093.7,1059.2v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C1094.2,1061.2,1093.7,1060.3,1093.7,1059.2z"/>
	</g>
	<g>
		<path class="st0" d="M1156.4,1025.5h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1025.5z"/>
		<path class="st0" d="M1159,1025.8v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C1159.9,1027.7,1159,1027.1,1159,1025.8z M1161.8,1025.9v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C1161.5,1026.6,1161.8,1026.2,1161.8,1025.9z"/>
	</g>
	<g>
		<path class="st0" d="M1072.1,1096.1h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C1070.4,1100.5,1071.2,1097.7,1072.1,1096.1z"/>
		<path class="st0" d="M1074.2,1101.8v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S1074.2,1102.9,1074.2,1101.8z
			 M1076.8,1098.5v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C1076.4,1099.3,1076.8,1098.9,1076.8,1098.5z"/>
	</g>
	<g>
		<path class="st0" d="M885.7,934l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V934z"/>
		<path class="st0" d="M888.2,939.2v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C888.7,941.2,888.2,940.3,888.2,939.2z"/>
		<path class="st0" d="M893,939.5v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S893,940.5,893,939.5z M895.5,936.1
			v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C895.2,936.9,895.5,936.6,895.5,936.1z"/>
	</g>
	<g>
		<path class="st0" d="M837.8,883.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V883.4z"/>
		<path class="st0" d="M840.4,888.8v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7H843v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C841.2,890.7,840.4,889.9,840.4,888.8z M843.1,888.9v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C842.9,889.6,843.1,889.2,843.1,888.9z"/>
		<path class="st0" d="M845.2,889.7l1.3-2.4c0.6-1.2,1-2.2,1-3.1V884c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V889.7z"/>
	</g>
	<g>
		<path class="st0" d="M807.7,1073v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C808.2,1075,807.7,1074.1,807.7,1073z"/>
		<path class="st0" d="M812.4,1074l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V1074z"/>
	</g>
	<g>
		<path class="st0" d="M785.8,1069.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1069.9z"/>
		<path class="st0" d="M790.6,1075h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1075z"/>
		<path class="st0" d="M795.5,1069.7h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C793.9,1074.1,794.7,1071.3,795.5,1069.7z"/>
	</g>
	<g>
		<path class="st0" d="M848.1,1051.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1051.7z"/>
		<path class="st0" d="M850.7,1057.2v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1H852c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C851.3,1058.9,850.7,1058.2,850.7,1057.2z"/>
		<path class="st0" d="M857.7,1056.8h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1056.8z"/>
	</g>
	<g>
		<path class="st0" d="M751.1,1101.4v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S751.1,1102.5,751.1,1101.4z
			 M753.6,1098.1v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7
			C753.2,1098.8,753.6,1098.5,753.6,1098.1z"/>
		<path class="st0" d="M755.8,1101.4v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C756.6,1103.1,755.8,1102.6,755.8,1101.4z M758.5,1101.4v-1.6c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C758.2,1102.1,758.5,1101.8,758.5,1101.4z M758.5,1097.5v-1.4c0-0.4-0.3-0.7-0.7-0.7
			c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C758.2,1098.2,758.5,1097.9,758.5,1097.5z"/>
	</g>
	<g>
		<path class="st0" d="M681.5,1100.6v-0.7h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S681.5,1101.7,681.5,1100.6z
			 M684,1097.3v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C683.7,1098,684,1097.7,684,1097.3z"/>
		<path class="st0" d="M688.6,1094.9h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C686.9,1099.3,687.7,1096.5,688.6,1094.9z"/>
	</g>
	<g>
		<path class="st0" d="M738.3,1120.9h-2.4v-1.1h3.7v1c-1.1,2-1.7,4.8-1.7,7.5h-1.3C736.7,1125.3,737.5,1122.5,738.3,1120.9z"/>
		<path class="st0" d="M741.1,1121.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1121.1z"/>
	</g>
	<g>
		<path class="st0" d="M595,978.7V978h1.2v0.6c0,0.4,0.2,0.7,0.7,0.7c0.5,0,0.7-0.3,0.7-0.8v-2.1c-0.3,0.4-0.7,0.6-1.1,0.6
			c-0.9,0-1.5-0.6-1.5-1.4v-2.1c0-1.1,0.8-1.8,2-1.8c1.2,0,1.9,0.8,1.9,1.9v4.8c0,1.3-0.8,2-2,2S595,979.7,595,978.7z M597.6,975.3
			v-1.9c0-0.4-0.2-0.7-0.7-0.7c-0.5,0-0.7,0.4-0.7,0.7v2c0,0.4,0.3,0.7,0.6,0.7C597.2,976.1,597.6,975.7,597.6,975.3z"/>
		<path class="st0" d="M599.8,978.5v-4.9c0-1.3,0.9-1.9,2-1.9c1.1,0,1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C600.6,980.4,599.8,979.6,599.8,978.5z M602.5,978.6v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C602.3,979.3,602.5,978.9,602.5,978.6z"/>
	</g>
	<g>
		<path class="st0" d="M614.1,975.6l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V975.6z"/>
		<path class="st0" d="M616.8,981v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C617.6,982.8,616.8,982.2,616.8,981z M619.5,981v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7
			C619.3,981.7,619.5,981.4,619.5,981z"/>
		<path class="st0" d="M621.7,981.1v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S621.7,982.2,621.7,981.1z
			 M624.3,981.1v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7C624.1,981.8,624.3,981.5,624.3,981.1
			z M624.3,977.2v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7
			C624.1,977.9,624.3,977.6,624.3,977.2z"/>
	</g>
	<g>
		<path class="st0" d="M469.9,1029l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1029z"/>
		<path class="st0" d="M473.2,1029l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1029z"/>
		<path class="st0" d="M475.7,1034.3v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C476.2,1036.3,475.7,1035.4,475.7,1034.3z"/>
	</g>
	<g>
		<path class="st0" d="M491.6,1051.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1051.4z"/>
		<path class="st0" d="M494.9,1051.4l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1051.4z"/>
	</g>
	<g>
		<path class="st0" d="M620.6,939.9l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V939.9z"/>
		<path class="st0" d="M623.2,946.2l1.3-2.4c0.6-1.2,1-2.2,1-3.1v-0.2c0-0.5-0.2-1-0.6-1c-0.5,0-0.6,0.4-0.6,0.8v0.5h-1.1v-0.5
			c0-1.1,0.7-1.9,1.8-1.9c1.2,0,1.8,0.8,1.8,2.1c0,1.5-0.7,2.9-1.4,4.1l-0.7,1.3v0h2.2v1.1h-3.6V946.2z"/>
		<path class="st0" d="M627.6,945.4V944c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C628.4,947.1,627.6,946.6,627.6,945.4z M630.3,945.4v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C630.1,946.1,630.3,945.8,630.3,945.4z M630.3,941.5v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6
			v1.4c0,0.4,0.3,0.7,0.7,0.7C630,942.2,630.3,941.9,630.3,941.5z"/>
	</g>
	<g>
		<path class="st0" d="M644.6,935.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V935.7z"/>
		<path class="st0" d="M647.1,941v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6V938h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C647.6,942.9,647.1,942.1,647.1,941z"/>
		<path class="st0" d="M651.8,941.2v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2V936c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7
			C652.6,942.9,651.8,942.4,651.8,941.2z M654.5,941.2v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6
			c0,0.4,0.3,0.7,0.7,0.7C654.2,941.9,654.5,941.6,654.5,941.2z M654.4,937.3v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6
			v1.4c0,0.4,0.3,0.7,0.7,0.7C654.2,938,654.4,937.7,654.4,937.3z"/>
	</g>
	<g>
		<path class="st0" d="M633.3,993.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V993.1z"/>
		<path class="st0" d="M635.8,998.4v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C636.3,1000.3,635.8,999.4,635.8,998.4z"/>
		<path class="st0" d="M640.5,998.6v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S640.5,999.8,640.5,998.6z
			 M643.2,998.6v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7C642.9,999.3,643.2,999,643.2,998.6z
			 M643.2,994.7v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4c0,0.4,0.3,0.7,0.7,0.7C642.9,995.4,643.2,995.1,643.2,994.7
			z"/>
	</g>
	<g>
		<path class="st0" d="M643,1010.8l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4H643V1010.8z"/>
		<path class="st0" d="M645.5,1016.1v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4c0-1.1-0.5-1.3-0.9-1.3h-0.6v-1.1
			h0.6c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C646,1018,645.5,1017.1,645.5,1016.1z"/>
		<path class="st0" d="M650.2,1016.2v-4.9c0-1.3,0.9-1.9,2-1.9s1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C651,1018,650.2,1017.3,650.2,1016.2z M652.9,1016.2v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C652.7,1017,652.9,1016.6,652.9,1016.2z"/>
	</g>
	<g>
		<path class="st0" d="M652.7,1055.1l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1055.1z"/>
		<path class="st0" d="M655.2,1060.4v-0.4h1.2v0.5c0,0.5,0.2,0.9,0.7,0.9c0.4,0,0.7-0.3,0.7-1.4s-0.5-1.3-0.9-1.3h-0.6v-1.1h0.6
			c0.5,0,0.8-0.4,0.8-1.4c0-0.9-0.3-1.2-0.7-1.2c-0.4,0-0.6,0.3-0.6,0.9v0.5h-1.2v-0.4c0-1.3,0.7-2,1.9-2c1.1,0,1.8,0.8,1.8,2.2
			c0,0.9-0.3,1.8-1,2v0c0.8,0.3,1.1,1.1,1.1,2c0,1.7-0.9,2.4-2,2.4C655.7,1062.4,655.2,1061.5,655.2,1060.4z"/>
		<path class="st0" d="M660,1060.5v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C660.8,1062.4,660,1061.8,660,1060.5z M662.7,1060.6v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1
			c0,0.4,0.2,0.7,0.7,0.7C662.5,1061.3,662.7,1060.9,662.7,1060.6z"/>
	</g>
	<g>
		<path class="st0" d="M624.6,1088.7l-0.7,0.2l-0.2-1l1.3-0.5h0.7v8.4h-1.2V1088.7z"/>
		<path class="st0" d="M629.4,1093.8H627v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1093.8z"/>
		<path class="st0" d="M632,1094.1v-5c0-1.2,0.9-1.8,2-1.8c1,0,2,0.6,2,1.8v5.1c0,1.2-0.9,1.8-2,1.8
			C632.9,1096,632,1095.3,632,1094.1z M634.8,1094.2v-5.1c0-0.4-0.3-0.7-0.7-0.7c-0.5,0-0.7,0.3-0.7,0.7v5.1c0,0.4,0.2,0.7,0.7,0.7
			C634.6,1094.9,634.8,1094.5,634.8,1094.2z"/>
	</g>
	<g>
		<path class="st0" d="M660.2,1030.8h-2.4v-1l2-5.4h1.3l-2.1,5.3h1.2v-1.5l1.2-0.8v2.3h0.6v1.1h-0.6v2h-1.2V1030.8z"/>
		<path class="st0" d="M662.8,1031.2v-1.4c0-0.6,0.3-1.1,0.7-1.3c-0.4-0.2-0.7-0.7-0.7-1.2v-1.3c0-1.1,0.8-1.7,1.9-1.7
			c1,0,1.9,0.6,1.9,1.7v1.3c0,0.6-0.2,1-0.7,1.3c0.4,0.2,0.8,0.7,0.8,1.2v1.5c0,1.1-0.9,1.7-2,1.7S662.8,1032.4,662.8,1031.2z
			 M665.5,1031.2v-1.5c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v1.6c0,0.4,0.3,0.7,0.7,0.7
			C665.2,1031.9,665.5,1031.6,665.5,1031.2z M665.5,1027.3v-1.4c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.6v1.4
			c0,0.4,0.3,0.7,0.7,0.7C665.2,1028.1,665.5,1027.7,665.5,1027.3z"/>
	</g>
	<g>
		<path class="st0" d="M578.1,995.1v-4.9c0-1.3,0.9-1.9,2-1.9s1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C578.8,997,578.1,996.2,578.1,995.1z M580.7,995.2v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8c0,0.4,0.2,0.7,0.7,0.7
			C580.5,995.9,580.7,995.5,580.7,995.2z"/>
	</g>
	<g>
		<path class="st0" d="M593.1,1009.3v-0.7h1.2v0.6c0,0.4,0.2,0.8,0.7,0.8c0.4,0,0.7-0.3,0.7-1v-1.6c0-0.6-0.2-0.9-0.7-0.9
			c-0.3,0-0.5,0.2-0.7,0.5l-1.1-0.1l0.2-4.4h3.3v1.1h-2.3c0,0,0,1.6-0.1,2.3c0.3-0.3,0.7-0.4,1.1-0.4c0.8,0,1.5,0.5,1.5,1.7v1.9
			c0,1.3-0.8,2-2,2C593.8,1011.1,593.1,1010.4,593.1,1009.3z"/>
		<path class="st0" d="M597.9,1009.2v-4.9c0-1.3,0.9-1.9,2-1.9s1.8,0.7,1.8,1.7v0.7h-1.2v-0.6c0-0.4-0.2-0.7-0.7-0.7
			c-0.5,0-0.7,0.4-0.7,0.8v2.1c0.3-0.3,0.7-0.5,1.2-0.5c0.9,0,1.4,0.5,1.4,1.4v2.1c0,1.1-0.8,1.8-2,1.8
			C598.7,1011.1,597.9,1010.3,597.9,1009.2z M600.6,1009.3v-2c0-0.4-0.3-0.6-0.6-0.6c-0.4,0-0.7,0.4-0.7,0.8v1.8
			c0,0.4,0.2,0.7,0.7,0.7C600.4,1010,600.6,1009.7,600.6,1009.3z"/>
	</g>
</g>
</svg>
