jQuery(function($) {
$(document).ready(function()  {


	// Overlay dropdown menu

	function showNav() {

		if ( $(window).width() > 1024 ) {
			// Clone and move to overlay
			var menu = ( !$('.nav-overlay .sub-menu').length ) ? $('.dropdown .sub-menu').clone() : '';
			$('.menu-item.dropdown > a').addClass('active');
			$('.nav-overlay').addClass('toggled').append( menu );
		}
	}

	function hideNav() {

		if ( $(window).width() > 1024 ) {
			// Hide overlay and destroy clone
			$('.dropdown a').removeClass('active');
			$('.nav-overlay').removeClass('toggled').find('.sub-menu').remove();
		}
	}

	$('.menu-item.dropdown > a').hover( showNav, null);
	$('.menu-item').not('.dropdown').hover(hideNav);
	$('.nav-overlay').on('mouseleave', '.sub-menu', hideNav);


	// Overlay for homepage video and other things (map)

	$('.overlay-toggle').on('click', function(e) {

		var clone = $( $(this).attr('data-target') ).clone();
		$('.uber-overlay').addClass('toggled').append(clone);

		// Homepage
		if ( $('body').hasClass('home') && $(window).width() > 1024 ) {
			$('.homepage-hero__video').get(0).pause();
		};
	});

	$('.uber-overlay').on('click touchstart', function(e) {

		$('.uber-overlay').removeClass('toggled').children().remove();

		// Homepage
		if ( $('body').hasClass('home') && $(window).width() > 1024 ) {
			$('.homepage-hero__video').get(0).play();
		};
	});

	// Mobile overlay close
	$('.uber-overlay').on('mouseover', function(e) {

		// Don't trigger on iframe click
		var sender = e.target;
		if ( $(sender).prop('tagName') !== 'IFRAME' && $(window).width() <= 1024 ) {
			$('.uber-overlay').removeClass('toggled').children().remove();
		}
	});


	// Mobile nav

	$('.nav__hamburger').on('click', function(e) {

		$(this).toggleClass('toggled');

		if ( $(this).hasClass('toggled') ) {

			$('.nav-overlay').append('<div class="nav-overlay__container">');

			$('.nav__menu').each(function() {

				var menu = $(this).clone();
				$('.nav-overlay__container').append( menu );

			});

			$('.nav-overlay').addClass('toggled');
			$('.nav').addClass('mobile-toggled');

		} else {

			$('.nav-overlay').removeClass('toggled').find('.nav-overlay__container').remove();
			$('.nav').removeClass('mobile-toggled');
		}
	});


	// Events on resize

	var mobileMode = $(window).width() <= 1024;
	$(window).on('resize', function() {

		// Reset overlay if moving between desktop and tablet/mobile views
		if ( !mobileMode && $(window).width() <= 1024 ) {
			mobileMode = true;
			$('.dropdown a').removeClass('active');
			$('.nav-overlay').removeClass('toggled').find('.sub-menu').remove();
		} else if ( mobileMode && $(window).width() > 1024 ) {
			mobileMode = false;
			$('.nav-overlay').removeClass('toggled').find('.nav-overlay__container').remove();
			$('.nav__hamburger').removeClass('togglde');
			$('.nav').removeClass('mobile-toggled');
		}
	});


	// Mini nav on scroll

	function minifyNav() {

		// Minify nav on scroll
		if ( $('.nav').hasClass('mini') == false && $(document).scrollTop() > 100 ) {
			$('.nav, .nav-overlay').addClass('mini');
		} else if ( $('.nav').hasClass('mini') && $(document).scrollTop() <= 100 ) {
			$('.nav, .nav-overlay').removeClass('mini');
		}
	};

	minifyNav();
	$(window).on('scroll', minifyNav);

	// Building page

	$('.building-carousel__container').slick({
		respondTo: window,
		prevArrow: $('.building-carousel__controls .prev'),
		nextArrow: $('.building-carousel__controls .next')
	});

	$('.building-marker a').on('mouseenter touchstart', function(e) {
		$(this).addClass('hovered');
		e.preventDefault();
	});

	$('.building-marker a').on('mouseleave touchend', function(e) {
		$(this).removeClass('hovered');
		e.preventDefault();
	});


	// Gallery page

	if ( $('body').hasClass('page-template-page-gallery') ) {

		$(function() {
			$('a[href*=#]:not([href=#])').click(function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
			  		scrollTop: target.offset().top - 78
						}, 1000);
						return false;
					}
				}
			});
		});

		$('.carousel').each(function (idx, item) { // can be used to initialize all carousels
			var carouselId = "carousel" + idx;
							this.id    = carouselId;
							nextSlide  = $(this).parent().find('.next'),
							prevSlide  = $(this).parent().find('.prev');

			$(this).slick({
				slide: "#" + carouselId +" img",
				dots: true,
				respondTo: window,
				prevArrow: prevSlide,
				nextArrow: nextSlide
			});
		});
		
		embedpano({xml:"/wp-content/themes/twentyfivekent/panos/interior.xml", id:"interior-pano", target:"interior", html5:"only", mobilescale:1.0, mwheel:false, passQueryParameters:true});

		embedpano({xml:"/wp-content/themes/twentyfivekent/panos/exterior.xml", id:"exterior-pano", target:"exterior", html5:"only", mobilescale:1.0, mwheel:false, passQueryParameters:true});

		embedpano({xml:"/wp-content/themes/twentyfivekent/panos/outdoor.xml", id:"outdoors-pano", target:"outdoor", html5:"only", mobilescale:1.0, mwheel:false, passQueryParameters:true});
	}

	$('.gallery-interiors__wall, .gallery-exteriors__wall, .gallery-outdoors__wall').on('click', function() {

		if ( $(window).width() >= 768 ) {

			$(this).fadeOut('slow', function(){

				$(this).parent().addClass('active');
				$(this).remove();

			});

		} else if ( $(window).width() < 768 ) {

			$(this).addClass('closed');
			$(this).prev().addClass('open');

		}

	});

	$('.gallery-interiors__close, .gallery-exteriors__close, .gallery-outdoors__close').on('click', function() {

		$(this).parent().removeClass('open');
		$(this).parent().next().removeClass('closed');

	});

	// Neighborhood page

	// Initialize hotspots map on desktop
	if ( $('body').hasClass('page-template-page-neighborhood') && $(window).width() > 1024 ) {

		$(function() {
	   var $section = $('#hotspots');
	   var $panzoom = $section.find('.neighborhood-hotspots__container').panzoom({
	     $zoomIn: $section.find(".zoom-in"),
	     $zoomOut: $section.find(".zoom-out"),
	     $zoomRange: $section.find(".zoom-range"),
	     $reset: $section.find(".reset"),
	     startTransform: 'scale(1.2) matrix(1, 0, 0, 1, 0, -300)',
	     increment: 0.33,
	     minScale: 0.5,
	     maxScale: 2,
	     contain: 'automatic'
	   }).panzoom('zoom', true);
	 });

	 $(function() {
	   var $section = $('#transit');
	   var $panzoom = $section.find('.neighborhood-transit__container').panzoom({
	     $zoomIn: $section.find(".zoom-in"),
	     $zoomOut: $section.find(".zoom-out"),
	     $zoomRange: $section.find(".zoom-range"),
	     $reset: $section.find(".reset"),
	     startTransform: 'scale(1.2)',
	     increment: 0.33,
	     minScale: 0.5,
	     maxScale: 2,
	     contain: 'automatic'
	   }).panzoom('zoom', true);
	 });

	}

	// Open the modal or download map
 $('.neighborhood-hotspots__launch').on('click', function(e) {
 	if ( $(window).width() > 1024 ) {
	 	$('.neighborhood-hotspots').addClass('open').css('opacity', '1');
	 	e.preventDefault();
	 } else if ( $(window).width() <= 1024 )  {
	 	window.open('/wp-content/themes/twentyfivekent/images/hotspotsmap.pdf');
	 }
 });

 $('.neighborhood-transit__launch').on('click', function(e) {
 	if ( $(window).width() > 1024 ) {
	 	$('.neighborhood-transit').addClass('open').css('opacity', '1');
	 	e.preventDefault();
	 } else if ( $(window).width() <= 1024 )  {
	 	window.open('/wp-content/themes/twentyfivekent/images/25Kent_Transit_Map.svg');
	 }
 });

 // Close the modal
 $('.neighborhood-hotspots__close').on('click', function(e) {
 	$('.neighborhood-hotspots').css('opacity', '0').removeClass('open');

 	// Reset filter settings
 	$('.neighborhood-hotspots__filters a').parent().siblings().removeClass('active');
 	$('.neighborhood-hotspots__filters a.all').parent().addClass('active');
 	$(all).css('opacity', '1');

 	e.preventDefault();
 });

 $('.neighborhood-transit__close').on('click', function(e) {
 	$('.neighborhood-transit').css('opacity', '0').removeClass('open');

 	e.preventDefault();
 });

 var all = $('.st1, .st2, .st3, .st4, .st5, .st6')

 $('.neighborhood-hotspots__filters a').on('click', function(e) {

 	// Get filter index and markers
 	var filterIndex = parseInt( $(this).attr('data-filter') );

 	// Give filter button 'active' class
 	$(this).parent().siblings().removeClass('active');
 	$(this).parent().addClass('active');

 	if( $(this).hasClass('all') ) { // Default Setting
 		$(all).parent().css({
 			opacity: '1',
 			display: 'block'
 		});
 	} else { // Use filter index to show markers
 		$(all).parent().css({
 			opacity: '0',
 			display: 'none'
 		});
 		$('.st'+filterIndex+'').parent().css({
 			opacity: '1',
 			display: 'block'
 		});
 	}

 	e.preventDefault();
 });

	$('.neighborhood-hotspots__marker').mousemove(function(e) {
		if ($(this).css('opacity').length == 1 ) {
			$('.neighborhood-hotspots__tooltip').offset({ top: e.pageY, left: e.pageX });
		}
	});

 $('.neighborhood-hotspots__marker').mouseenter(function(e) {
 	var hotspot = $(this).attr('data-hotspot');
 	if ($(this).css('opacity').length == 1 ) {
	  $('.neighborhood-hotspots__tooltip').css({
	  	opacity: '1',
	  }).find('p').text(hotspot);
 	}
 });

	$('.neighborhood-hotspots__marker').mouseleave(function(e) {
		if ($(this).css('opacity').length == 1 ) {
	  $('.neighborhood-hotspots__tooltip').css({
	  	opacity: '0'
	  });
 	}
 });

	// Floor Plans page

	// Stats section

	function moveStats() {
	 if ( $(window).width() > 768 ) {
			$('.floorplan-details__stats').each(function() {
				$(this).appendTo($(this).siblings('.floorplan-details__info'));
			});
		}	else if ( $(window).width() <= 768 ) {
			$('.floorplan-details__stats').each(function() {
				$(this).appendTo($(this).closest('.floorplan-details'));
			});
		}
	}

	if ( $('body').hasClass('page-template-page-floor-plan')) { // finish this so it limits firing at certain points
		moveStats();
		$(window).on('resize', function() {
			moveStats();
		});
	}


	// Floor hover

	function highlightFloor( floorIndex ) {

		// Remove active nav class
		$('.floorplan-interactive__nav-item').removeClass('active');

		// Assign floor classes
		$('.floors-stack__image').each(function() {

			var thisIndex = parseInt( $(this).attr('data-floor') );

			if ( thisIndex < floorIndex ) {

				$(this).addClass('below');
				$(this).removeClass('above');

			} else if ( thisIndex > floorIndex ) {

				$(this).removeClass('below');
				$(this).addClass('above');

			} else {

				// Selected floor
				$(this).removeClass('above below');
				$('.floorplan-interactive__nav-item[data-floor="'+thisIndex+'"]').addClass('active');

			}

		});
	};

	function unhighlightFloor() {

		if ( $('.floorplan-interactive__nav').hasClass('hidden') == false  ) {

			// Reset
			$('.floorplan-interactive__nav-item').removeClass('active');
			$('.floorplan-interactive__nav-item[data-floor="0"]').addClass('active');
			$('.floors-stack__image').removeClass('above below');

		}
	}

	$('.floors-stack__image, .floorplan-interactive__nav-item').on('hover', function() {

		var targetFloor = parseInt( $(this).attr('data-floor') );

		if ( targetFloor !==0 && $(window).width() > 1008 ) {
			highlightFloor( targetFloor );
		} else {
			unhighlightFloor();
		}

		

	});

	$('.floors-stack__image, .floorplan-interactive__nav').on('mouseleave', unhighlightFloor);


	// Floor selection

	function selectFloor( floorIndex ) {

		// Show arrows
		$('.floors-arrows').fadeIn(300);

		// Add unselected class to nav, hide other floors
		$('.floors-stack__image').removeClass('selected faded').addClass('unselected');
		$('.floorplan-interactive__detail-block').removeClass('selected');
		$('.floorplan-interactive__nav').addClass('hidden');
		$('.floors-stack__image.birds-eye').fadeOut(300);

		// Show details for floor
		$('.floors-stack__image[data-floor="'+floorIndex+'"]').removeClass('unselected').addClass('selected');
		$('.floorplan-interactive__detail-block[data-floor="'+floorIndex+'"]').addClass('selected');

		// Change all floors button text
		$('.floorplan-interactive__nav-item[data-floor="0"]').text('See All Floors');

		// Disable buttons if necessary
		if ( floorIndex + 1 > $('.floors-stack__image.birds-eye').length ) {
			$('.floors-arrows__button.up').addClass('disabled');
		} else {
			$('.floors-arrows__button.up').removeClass('disabled');
		}
		if ( floorIndex - 1 <= 0 ) {
			$('.floors-arrows__button.down').addClass('disabled');
		} else {
			$('.floors-arrows__button.down').removeClass('disabled');
		}

	};

	function unselectFloors() {

		// Hide arrows
		$('.floors-arrows').fadeOut(300);

		// Remove unselected class from nav, unselect floors
		$('.floors-stack__image').removeClass('unselected selected faded');
		$('.floorplan-interactive__detail-block').removeClass('selected');
		$('.floorplan-interactive__nav').removeClass('hidden');
		$('.floors-stack__image.birds-eye').fadeOut(300);
		$('.birds-eye-toggle .button__text').text('Bird\'s-Eye View');

		// Change all floors button text
		$('.floorplan-interactive__nav-item[data-floor="0"]').text('All Floors');

		unhighlightFloor();

	}

	$('.floors-stack__image, .floorplan-interactive__nav-item').on('click', function() {

		if ( $(this).hasClass('unselected') == false ) {
			var targetFloor = parseInt( $(this).attr('data-floor') );
			if ( targetFloor !== 0 ) selectFloor( targetFloor );
		}

	});

	$('.floorplan-interactive__nav-item[data-floor="0"]').on('click', unselectFloors);


	// Floor bird's-eye view
	$('.birds-eye-toggle').on('click', function() {

		var floorIndex = parseInt( $(this).closest('.floorplan-interactive__detail-block').attr('data-floor') );

		if ( $('.floors-stack__image:not(.birds-eye)[data-floor="'+floorIndex+'"]').hasClass('faded') )
			$(this).find('.button__text').text('Bird\'s-Eye View');
		else
			$(this).find('.button__text').text('Isometric View');

		$('.floors-stack__image.birds-eye[data-floor="'+floorIndex+'"]').fadeToggle(300);
		$('.floors-stack__image:not(.birds-eye)[data-floor="'+floorIndex+'"]').toggleClass('faded');

	});

	// Floor plan arrows

	$('.floors-arrows__button').on('click', function() {

		var currentFloor = parseInt( $('.floorplan-interactive__detail-block.selected').attr('data-floor') );

		if ( $(this).hasClass('up') && currentFloor + 1 <= $('.floors-stack__image.birds-eye').length ) {
			highlightFloor( currentFloor + 1 );
			selectFloor( currentFloor + 1 );
		} else if ( $(this).hasClass('down') && currentFloor - 1 > 0 ) {
			highlightFloor( currentFloor - 1 );
			selectFloor( currentFloor - 1 );
		}

	});

	// Floor plan view for Mobile 

	$(".birds-eye-toggle-mobile").click(function(){

		var birdEyeView = $(".birds-eye"),
			dropdownContents = $(this).parent(),
			textToBeChanged = $(this).find(".button__text");

		$(this).parent().toggleClass("change");

		if ( dropdownContents.hasClass("change") ){

			dropdownContents.find(".birds-eye").fadeIn(300);
			dropdownContents.find(".isometric").fadeOut(0);
			textToBeChanged.text("Isometric View");

		} else {

			dropdownContents.find(".birds-eye").fadeOut(0);
			dropdownContents.find(".isometric").fadeIn(300);
			textToBeChanged.text("Bird\'s-Eye View");
		}

	});


	$("#accordion").accordion({
		heightStyle: "content",
		collapsible: true,
		active: false,
		activate: function( event, ui ) {
			if(!$.isEmptyObject(ui.newHeader.offset())) {
				$('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top - $(".nav").outerHeight() }, 'fast');
			}
		}
	});


});
});