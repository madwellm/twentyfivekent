<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_57e007b313337',
	'title' => 'Footer Contact CTA',
	'fields' => array (
		array (
			'key' => 'field_57e007bcb0d52',
			'label' => 'CTA Header',
			'name' => 'cta_header',
			'type' => 'text',
			'instructions' => 'e.g. "Like what you see so far?"',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Like what you see so far?',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_57e00812b0d53',
			'label' => 'CTA Button Text',
			'name' => 'cta_button_text',
			'type' => 'text',
			'instructions' => 'e.g. "Let\'s talk" (no punctuation)',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Let\'s talk',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 100,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;